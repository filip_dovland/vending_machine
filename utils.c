#include "em_device.h"
#include "em_timer.h"
#include "em_cmu.h"
#include "utils.h"

#define HF_FREQ 48000000
#define LF_FREQ 32768

extern volatile uint32_t msTicks;

void delayUs(int us)
{
  uint32_t top =  us * (HF_FREQ / 1000000);
  
  TIMER3->CNT = 0;
  TIMER3->CMD = TIMER_CMD_START;
  while(TIMER3->CNT < top);
  TIMER3->CMD = TIMER_CMD_STOP;
}

void delayMs(int ms)
{
	uint32_t timeout = msTicks + ms;
	while ( msTicks < timeout );
}

void initDelay(void)
{
  CMU_ClockEnable(cmuClock_TIMER3, true);
  CMU_ClockEnable(cmuClock_CORELE, true);
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
  CMU_ClockEnable(cmuClock_RTC, true); 
}
  
