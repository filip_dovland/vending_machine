#ifndef _CONFIG_H_
#define _CONFIG_H_

// Rename this file to config.h and configure parameters

#define WIFI_SSID    "my ssid name"
#define WIFI_PASSWD  "super secret password"

// Buttons and WIFI_INT drive interrupts. They must have unique pin numbers. 

#define BUTTON_UP_PORT     gpioPortC
#define BUTTON_UP_PIN      5
#define BUTTON_DOWN_PORT   gpioPortA
#define BUTTON_DOWN_PIN    13
#define BUTTON_CANCEL_PORT gpioPortB
#define BUTTON_CANCEL_PIN  11
#define BUTTON_OK_PORT     gpioPortB
#define BUTTON_OK_PIN      12

#define WIFI_INT_PORT      gpioPortC
#define WIFI_INT_PIN       0

#define WIFI_EN_PORT       gpioPortC
#define WIFI_EN_PIN        3

#define WIFI_MOSI_PORT     gpioPortD
#define WIFI_MOSI_PIN      0
#define WIFI_MISO_PORT     gpioPortD
#define WIFI_MISO_PIN      1
#define WIFI_CLK_PORT      gpioPortD
#define WIFI_CLK_PIN       2
#define WIFI_CS_PORT       gpioPortD
#define WIFI_CS_PIN        3

#define RFID_RX_PORT       gpioPortE
#define RFID_RX_PIN        1

#endif
