#ifndef _WEB_FRONTEND_H_
#define _WEB_FRONTEND_H_


#define MAX_QUERY_LEN 40
#define HTTP_HEADER_200_OK       "HTTP/1.1 200 OK\nContent-Type: text/html\n\n"
#define HTTP_HEADER_400_BAD_REQ  "HTTP/1.1 400 Bad Request\n\n"
#define HTTP_HEADER_404          "HTTP/1.1 404 Not Found\n\n"

void queryRoot(int32_t socket);
void queryUsers(int32_t socket);
void queryItems(int32_t socket);
void queryTransactions(int32_t socket);

#endif
