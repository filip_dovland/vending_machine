#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "em_device.h"
#include "web_frontend.h"
#include "socket.h"
#include "transactions.h"


#define RESPONSE_BUFFER_LENGTH 1024

const char *HTML_ROOT_BODY = "<html><body><h2>Gecko Vending Machine</h2>"
    "<p><a href='/users'>users</a><p>"
    "<p><a href='/items'>products</a></p>"
    "<p><a href='/transactions'>transactions</a></p>"
    "</body></html>";

const char *HTML_USERS_HEADER = "<html><body><h2>User List</h2>"
    "<table cellpadding='0' cellspacing='0'><tr><th>id</th><th>name</th><th>balance</th></tr>";

const char *HTML_USERS_FOOTER = "</table></body></html>";

const char *HTML_ITEMS_HEADER = "<html><body><h2>Product List</h2>"
    "<table cellpadding='0' cellspacing='0'><tr><th>id</th><th>name</th><th>price</th></tr>";

const char *HTML_ITEMS_FOOTER = "</table></body></html>";

const char *HTML_TRANSACTIONS_HEADER = "<html><body><h2>Transactions List</h2>"
    "<table border='0' cellpadding='2' cellspacing='5'><tr><th>user</th><th>item</th><th>price</th></tr>";

const char *HTML_TRANSACTIONS_FOOTER = "</table></body></html>";


static char responseBuffer[RESPONSE_BUFFER_LENGTH];
extern Database database;


void sendHttpResponse(int32_t socket, const char *body)
{
  int n;
  n = send(socket, body, strlen(body), 0);
}

void queryRoot(int32_t socket)
{
  sendHttpResponse(socket, HTTP_HEADER_200_OK);
  sendHttpResponse(socket, HTML_ROOT_BODY);
}

void queryUsers(int32_t socket)
{
  int i;
  sendHttpResponse(socket, HTTP_HEADER_200_OK);
  sendHttpResponse(socket, HTML_USERS_HEADER);
  for ( i=0; i<database.numUsers; i++ ) {
    sprintf(responseBuffer, "<tr><td>%d</td><td>%s</td><td>%d</td></tr>", i, database.users[i].name, database.users[i].balance);
    sendHttpResponse(socket, responseBuffer);
  }
  sendHttpResponse(socket, HTML_USERS_FOOTER);
}

void queryItems(int32_t socket)
{
  int i;
  sendHttpResponse(socket, HTTP_HEADER_200_OK);
  sendHttpResponse(socket, HTML_ITEMS_HEADER);
  for ( i=0; i<database.numItems; i++ ) {
    sprintf(responseBuffer, "<tr><td>%d</td><td>%s</td><td>%d</td></tr>", i, database.items[i].name, database.items[i].price);
    sendHttpResponse(socket, responseBuffer);
  }
  sendHttpResponse(socket, HTML_ITEMS_FOOTER);
}

void queryTransactions(int32_t socket)
{
  int i;
  User *user;
  Item *item;
  char *userName;
  char *itemName;

  sendHttpResponse(socket, HTTP_HEADER_200_OK);
  sendHttpResponse(socket, HTML_TRANSACTIONS_HEADER);

  i = database.transactionStart;
  while ( i != database.transactionEnd ) {
  	Transaction *tran = &database.transactions[i];

  	if ( tran->user >= 0 ) {
  		user = &database.users[ tran->user ];
  	} else {
  		user = NULL;
  	}

  	if ( tran->item >= 0 ) {
  		item = &database.items[ tran->item ];
  	} else {
  		item = NULL;
  	}

  	if ( user != NULL ) {
  		userName = user->name;
  	} else {
  		userName = "[deleted]";
  	}

  	if ( item != NULL ) {
  		itemName = item->name;
  	} else {
  		itemName = "";
  	}

    sprintf(responseBuffer, "<tr><td>%s</td><td>%s</td><td>%d</td></tr>", userName, itemName, tran->value);
    sendHttpResponse(socket, responseBuffer);

    if ( ++i > MAX_TRANSACTIONS ) {
    	i = 0;
    }
  }
  sendHttpResponse(socket, HTML_TRANSACTIONS_FOOTER);
}

