# Vending Machine

This repository contains source code for a simple vending machine.The machine allows users to authenticate with RFID cards and buy products with a simple user interface. Balances, prices and transactions can be viewed over HTTP. A telnet interface can be used to administer the machine.

## Usage

### Buying items

The user logs in by tapping their RFID card. A welcome message appears with their current balance. They can then select a product by using the up/down buttons and confirm with the right button. The left button cancels any dialog.

### Administration

At the welcome screen, press the left butten three times to show the current IP. A telnet connection is available at port 23 when connected to WiFi. Simple commands are available for adding products, users, crediting accounts etc. Type 'help' to get a list of commands.

### Web Server

A simple web server is available at port 80 which shows current prices, balances and the last transactions. 

### Link Card

When adding a new user, the account must be linked to a card. After adding the user account via the telnet interface, tap a new card to the RFID reader. This will show a list of all non-linked users. Use up/down to select a user and press the OK button to confirm. 

## Hardware

The hardware consists of:

 * [EFM32GG-STK3700 Giant Gecko Starter Kit](http://www.silabs.com/products/mcu/32-bit/Pages/efm32gg-stk3700.aspx)
 * [CC3000 Breakout Board](https://www.sparkfun.com/products/12072)
 * [20x4 Character LCD](https://www.sparkfun.com/products/256)
 * [ID-12LA RFID Reader](https://www.sparkfun.com/products/11827)
 * Potentiometer
 * Push buttons and wires

The wiring should be pretty self-explanatory by inspecting config_template.h and lcd_driver.h. The central part is the EFM32GG-STK3700 board. Everything is connected with standard hookup wire and almost no soldering is required (mostly soldering on pin headers to the boards). A potentiometer is connected to the LCD to adjust contrast. It is a good idea to put a large capacitor on the supply to the RFID reader to reduce supply ripple. Without this, the range was reduced.

![hardware.jpeg](https://bitbucket.org/repo/LMpgzM/images/731002620-hardware.jpeg)

## Source Code

In addition to the source code in this repository you need the EFM32 SDK, which you get through [Simplicity Studio](http://www.silabs.com/products/mcu/Pages/simplicity-studio.aspx).

The main components of the software is:

 * WiFi driver (CC3000 Host Driver ported to run on the EFM32)
 * LCD Driver
 * User Interface (LCD screens and push button handling)
 * HTTP and telnet servers
 * Database

Out of reset, the machine will first try to connect to WiFi before entering the main loop which serves the UI and HTTP/telnet servers. If the WiFi connection fails, the UI can still be used to buy products, but the administration interface will be unavailable. The database is stored on the last pages of internal Flash in the EFM32GG with a simple redundancy scheme. Each time the database is updated it is written to one of two locations (whicever is oldest/invalid) and appended it with a CRC. If the database update fails for some reason (e.g. power loss during Flash write) the software can always fall back to a valid database.


## In Action

Here it is in our office. 

![vending_machine.jpg](https://bitbucket.org/repo/LMpgzM/images/2040169473-vending_machine.jpg)

