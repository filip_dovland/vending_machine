#ifndef _WIFI_CONTROL_H_
#define _WIFI_CONTROL_H_

void startListenServers(void);
void initWifi(void);
bool startWifi(void);
void handleNetworkConnections(void);
void printIpAddr(uint8_t *ipAddr, bool reverse);

#endif
