#ifndef _SPI_H_
#define _SPI_H_


typedef void (*gcSpiHandleRx)(void *p);
typedef void (*gcSpiHandleTx)(void);


void SpiOpen(gcSpiHandleRx pfRxHandler);
void SpiClose(void);
long SpiWrite(unsigned char *pUserBuffer, unsigned short usLength);

long readWlanInterruptPin();
void enableWlanInterrupt();
void disableWlanInterrupt();
void writeWlanPin(unsigned char val);
void wifiIrqHandler(void);
void SpiResumeSpi(void);

#endif
