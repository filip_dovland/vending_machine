#include "em_device.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "em_cmu.h"
#include "wifi_spi.h"
#include "cc3000_common.h"
#include "utils.h"
#include "ui.h"
#include "config.h"


//TODO enable NVIC

#define OPCODE_WRITE 0x01
#define OPCODE_READ  0x03

#define STATE_POWERUP     1
#define STATE_INITIALIZED 2
#define STATE_IDLE        3
#define STATE_READ        4

#define SPI_HEADER_SIZE 5


gcSpiHandleRx rxHandler;

static uint8_t rx_buffer[CC3000_MAXIMAL_RX_SIZE];
uint8_t wlan_tx_buffer[CC3000_MAXIMAL_RX_SIZE];

extern volatile sSimplLinkInformation tSLInformation;

static int spiState = STATE_POWERUP;

static bool firstWrite = true;

static volatile bool readPaused = false;


static void assertCs(void)
{
  GPIO_PinOutClear(WIFI_CS_PORT, WIFI_CS_PIN);
}

static void deassertCs(void)
{
  GPIO_PinOutSet(WIFI_CS_PORT, WIFI_CS_PIN);
}

void SpiRead(void)
{
  int i;
  uint16_t length;

  // Clear WIFI interrupt while processing
	GPIO ->IEN &= ~(1 << WIFI_INT_PIN);

  uint8_t header[5];

  header[0] = OPCODE_READ;
  header[1] = 0x00;
  header[2] = 0x00;
  header[3] = 0x00;
  header[4] = 0x00;

  assertCs();

  USART1->CMD = USART_CMD_CLEARRX;

  // Send header
  for ( i=0; i<SPI_HEADER_SIZE; i++ ) {
    while ( !(USART1->STATUS & USART_STATUS_TXBL) );
    USART1->TXDATA = header[i];
    while ( !(USART1->STATUS & USART_STATUS_RXDATAV) );
    rx_buffer[i] = USART1->RXDATA;
  }

  // Get payload length
  length = (rx_buffer[3] << 8) | rx_buffer[4];

  // Read payload
  for ( i=0; i<length; i++ ) {
    while ( !(USART1->STATUS & USART_STATUS_TXBL) );
    USART1->TXDATA = 0x00;
    while ( !(USART1->STATUS & USART_STATUS_RXDATAV) );
    rx_buffer[i] = USART1->RXDATA;
  }

  while ( !(USART1->STATUS & USART_STATUS_TXC) );

  deassertCs();
  
#ifdef DEBUG_PRINT
  printf("SPI READ  ");
  for (i=0; i<length; i++)
  {
  	printf("%02x ", rx_buffer[i]);
  }
  printf("\n");
#endif
  
  rxHandler(rx_buffer);
}


void SpiOpen(gcSpiHandleRx pfRxHandler)
{
  // Save pointer to RX handler
  rxHandler = pfRxHandler;
  CMU_ClockEnable(cmuClock_USART1, true);
  USART_InitSync_TypeDef usartInit = USART_INITSYNC_DEFAULT;

  usartInit.enable = usartEnable;
  usartInit.refFreq = 0;
  usartInit.baudrate = 4000000;
  usartInit.databits = usartDatabits8;
  usartInit.master = true;
  usartInit.msbf = true;
  usartInit.clockMode = usartClockMode1;
  usartInit.prsRxEnable = false;
  usartInit.autoTx = false;

  USART_InitSync(USART1, &usartInit);
  
  USART1->ROUTE = USART_ROUTE_LOCATION_LOC1
    | USART_ROUTE_CLKPEN  
    | USART_ROUTE_TXPEN
    | USART_ROUTE_RXPEN;
  
}

void SpiClose(void)
{
  CMU_ClockEnable(cmuClock_USART1, false);
}


long SpiWriteNormal(unsigned char *buffer, unsigned short length)
{
  int i;

  // TODO check for possible race condition between read/write requests
  disableWlanInterrupt();
  
  assertCs();

  //enableWlanInterrupt();

  // Wait for INT to be asserted 
  while ( readWlanInterruptPin() );
  
  enableWlanInterrupt();

  //if ( !readWlanInterruptPin() ) {

	for ( i=0; i<length; i++ ) {
		while ( !(USART1->STATUS & USART_STATUS_TXBL) );
		USART1->TXDATA = buffer[i];
	}

	while ( !(USART1->STATUS & USART_STATUS_TXC) );

	deassertCs();
  //}

  // Wait for INT to be deasserted
  //while ( !readWlanInterruptPin() );
  
  

  return 0;
}

long SpiWriteFirst(unsigned char *buffer, unsigned short length)
{
  int i;

  // Wait for INT to be asserted 
  while ( readWlanInterruptPin() );
  
  assertCs();
  
  delayUs(80);
  
  for ( i=0; i<4; i++ ) {
    while ( !(USART1->STATUS & USART_STATUS_TXBL) );
    USART1->TXDATA = buffer[i];
  }
  
  delayUs(80);
  
  for ( i=4; i<length; i++ ) {
    while ( !(USART1->STATUS & USART_STATUS_TXBL) );
    USART1->TXDATA = buffer[i];
  }
  
  while ( !(USART1->STATUS & USART_STATUS_TXC) );

  deassertCs();

  // Wait for INT to be deasserted
  while ( !readWlanInterruptPin() );
  
  return 0;
}


long SpiWrite(unsigned char *buffer, unsigned short length)
{
  // Add padding byte if payload length is even
  if ( !(length & 0x1) ) {
    length++;
  }

  buffer[0] = OPCODE_WRITE;
  buffer[1] = (length >> 8) & 0xFF;
  buffer[2] = length & 0xFF;
  buffer[3] = 0x00;
  buffer[4] = 0x00;  
  
  length += SPI_HEADER_SIZE;
  
#ifdef DEBUG_PRINT
  int i;
  printf("SPI WRITE ");
  for (i=0; i<length; i++)
  {
  	printf("%02x ", buffer[i]);
  }
  printf("\n");
#endif

  if ( firstWrite ) {
    firstWrite = false;
    return SpiWriteFirst(buffer, length);
  } else {
    return SpiWriteNormal(buffer, length);
  }
}


void SpiResumeSpi(void)
{
	// Enable interrupt when finished processing
	GPIO->IEN |= 1 << WIFI_INT_PIN;
}

long readWlanInterruptPin()
{
  return GPIO_PinInGet(WIFI_INT_PORT, WIFI_INT_PIN);
}

void enableWlanInterrupt()
{
  GPIO->IFC = 1 << WIFI_INT_PIN;
  GPIO->IEN |= (1 << WIFI_INT_PIN);
}

void disableWlanInterrupt()
{
  GPIO->IEN &= ~(1 << WIFI_INT_PIN);
}

void writeWlanPin(unsigned char val)
{
  if ( val ) {
    GPIO_PinOutSet(WIFI_EN_PORT, WIFI_EN_PIN);
  } else {
    GPIO_PinOutClear(WIFI_EN_PORT, WIFI_EN_PIN);
  }
}

void wifiIrqHandler(void)
{
	GPIO->IFC = 1 << WIFI_INT_PIN;
  if ( spiState == STATE_POWERUP ) {
    spiState = STATE_IDLE;
  } else if ( spiState == STATE_IDLE ) {
    SpiRead();
  }
}



/* void SpiResumeSpi(void); */
/* void SSIContReadOperation(void); */
/* void SpiTriggerRxProcessing(void); */
/* uint8_t get_spi_data_mode(void); */
/* uint8_t get_spi_bit_order(void); */
/* uint8_t get_spi_clock_div(void); */
/* void save_spi_params(void); */
/* void restore_spi_params(void); */
/* void cc3000_ISR(void); */
