#include <string.h>
#include <stdio.h>
#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "wlan.h"
#include "hci.h"
#include "wifi_spi.h"
#include "utils.h"
#include "common.h"
#include "socket.h"
#include "netapp.h"
#include "transactions.h"
#include "web_frontend.h"
#include "evnt_handler.h"
#include "telnet_cmd.h"
#include "config.h"

#include "wifi_control.h"

extern volatile uint32_t msTicks;


#define RX_BUFFER_SIZE 1024
#define TX_BUFFER_SIZE 1024

typedef struct _Server {
  sockaddr addr;
  uint16_t port;
  int32_t socket;
} Server;


/* Globals */

Server httpServer;
Server telnetServer;

int32_t socketInst;
volatile bool simpleConfigDone = false;
volatile bool unsolConnect = false;
volatile bool unsolDisconnect = false;
volatile bool dhcpDone = false;
volatile bool okToShutDown = false;
volatile bool eventPingReport = false;
volatile bool eventConnect = false;
volatile bool eventTcpCloseWait = false;

extern unsigned char wlan_tx_buffer[];

extern bool wifiConnected;

static char requestBuffer[RX_BUFFER_SIZE];

tNetappIpconfigRetArgs connInfo;


void wifiEventCallback(long event_type, char * data, unsigned char length )
{
  printf("Event callback\n");

  if (event_type == HCI_EVNT_WLAN_ASYNC_SIMPLE_CONFIG_DONE)
  {
    simpleConfigDone = true;
    printf("EVENT Simple config done\n");
  }
  else if (event_type == HCI_EVNT_WLAN_UNSOL_CONNECT)
  {
    unsolConnect = true;
    printf("EVENT Unsolicited Connect\n");
  }
  else if (event_type == HCI_EVNT_WLAN_UNSOL_DISCONNECT)
  {
    unsolDisconnect = true;
    wifiConnected = false;
    printf("EVENT Unsolicited Disconnect\n");
  }
  else if (event_type == HCI_EVNT_WLAN_UNSOL_DHCP)
  {
    printf("EVENT DHCP\n");
    dhcpDone = true;
  }
  else if (event_type == HCI_EVENT_CC3000_CAN_SHUT_DOWN)
  {
    okToShutDown = true;
    printf("EVENT OK to shut down\n");
  }
  else if (event_type == HCI_EVNT_WLAN_ASYNC_PING_REPORT)
  {
    eventPingReport = true;
  }
  else if(event_type == HCI_EVNT_CONNECT)
  {
    eventConnect = true;
    printf("EVENT Connect\n");
  }
  else if (event_type == HCI_EVNT_BSD_TCP_CLOSE_WAIT)
  {
    eventTcpCloseWait = true;
  }
  else
  {
    printf("EVENT UNHANDLED 0x%x\n", (unsigned int)event_type);
  }

}

void initWifi(void)
{
  printf("Initializing wifi\n");

  CMU_ClockEnable(cmuClock_GPIO, true);

  GPIO_PinModeSet(WIFI_CS_PORT, WIFI_CS_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(WIFI_INT_PORT, WIFI_INT_PIN, gpioModeInput, 0);
  GPIO_PinModeSet(WIFI_MOSI_PORT, WIFI_MOSI_PIN, gpioModePushPull, 0);
  GPIO_PinModeSet(WIFI_MISO_PORT, WIFI_MISO_PIN, gpioModeInput, 0);
  GPIO_PinModeSet(WIFI_CLK_PORT, WIFI_CLK_PIN, gpioModePushPull, 0);
  GPIO_PinModeSet(WIFI_EN_PORT, WIFI_EN_PIN, gpioModePushPull, 0);

  delayMs(1000);

  GPIO_ExtIntConfig(WIFI_INT_PORT, WIFI_INT_PIN, 0, false, true, true);

  /* Initialize CC3000 library - provide callback definitions */
  wlan_init(  wifiEventCallback,
              NULL,                    // No FW patch callback
              NULL,                    // No send driver patch callback
              NULL,                    // No send bootloader patch callback
              readWlanInterruptPin,
              enableWlanInterrupt,
              disableWlanInterrupt,
              writeWlanPin);

    /* CC3000 needs a delay before starting WLAN or it gets stuck sometimes */
    delayMs(100);

    /* Start CC3000 - asserts enable pin and blocks until init is complete */
    wlan_start(0);

    // Falling edge interrupt
    delayMs(10);


    /* Mask out non-required events */
    wlan_set_event_mask(HCI_EVNT_WLAN_KEEPALIVE | HCI_EVNT_WLAN_UNSOL_INIT);
}

void wifiConnectToAp(void)
{
  long status;

  status = wlan_ioctl_statusget();
  if ( status == WLAN_STATUS_CONNECTED ) {
    printf("Already connected to access point\n");
    return;
  }

	printf("Connecting to access point\n");

  /* Set connection profile to manual (no fast or auto connect) */
  if (wlan_ioctl_set_connection_policy(0, 0, 0) != CC3000_SUCCESS) {
      printf("Failed to set connection policy\n");
  }

  delayMs(10);


  if ( strlen(WIFI_PASSWD) == 0 ) {
  	status = wlan_connect(WLAN_SEC_UNSEC, WIFI_SSID, strlen(WIFI_SSID), 0, (uint8_t *)WIFI_PASSWD, strlen(WIFI_PASSWD));
  } else {
  	status = wlan_connect(WLAN_SEC_WPA2, WIFI_SSID, strlen(WIFI_SSID), 0, (uint8_t *)WIFI_PASSWD, strlen(WIFI_PASSWD));
  }


  if ( status != CC3000_SUCCESS ) {
    printf("Failed to connect to access point\n");
    return;
  }
}


int available()
{
    fd_set readsds;
    timeval timeout;

    /* We need something in readsds to tell select() to watch read sockets */
    memset(&readsds, 1, sizeof(readsds));

    /* Minimum timeout for select() is 5ms */
    timeout.tv_sec = 0;
    timeout.tv_usec = 5000;

    /* Call select() to see if there is any data waiting */
    int ret = select(socketInst + 1, &readsds, NULL, NULL, &timeout);

    /* If select() returned anything greater than 0, there's data for us */
    if (ret > 0) {
        return true;
    } else {
        return false;
    }
}

int readInput()
{
    uint8_t b;

    if (recv(socketInst, &b, 1, 0) > 0) {
        return b;
    } else {
        return -1;
    }
}

void printIpAddr(uint8_t *ipAddr, bool reverse)
{
  int i;

  if ( reverse ) {
    for ( i=0; i<4; i++ ) {
      printf("%d", ipAddr[i]);
      if ( i < 3 ) {
        printf(".");
      }
    }
  } else {

    for ( i=3; i>=0; i-- ) {
      printf("%d", ipAddr[i]);
      if ( i > 0 ) {
        printf(".");
      }
    }
  }
  printf("\n");
}

bool startWifi(void)
{
  wifiConnectToAp();

  // Time out after 5 s
  uint32_t timeout = msTicks + 5000;
  while (!dhcpDone && msTicks < timeout);

  if (!dhcpDone) {
  	printf("Failed to get IP\n");
  	return false;
  }

  int i;
  netapp_ipconfig(&connInfo);

  printf("Connection info:\n");
  printf("SSID: %s\n", connInfo.uaSSID);
  printf("MAC:  ");
  for (i=0; i<6; i++) {
    printf("%02x", connInfo.uaMacAddr[i]);
    if ( i < 5 ) {
      printf(":");
    }
  }
  printf("\n");
  printf("IP:   "); printIpAddr(connInfo.aucIP, false);
  printf("DCHP: "); printIpAddr(connInfo.aucDHCPServer, false);

  return true;
}


bool startListenServer(Server *server)
{
  int i;

  printf("Starting listen server on port %d\n", server->port);

  /* Create a socket */
  server->socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (server->socket == -1) {
    printf("Failed to acquire http server socket\n");
    return false;
  }

    /* Set address family to AF_INET (only one that works right now) */
   server->addr.sa_family = AF_INET;

    /* Fill out the destination port */
   server->addr.sa_data[0] = (server->port & 0xFF00) >> 8;
   server->addr.sa_data[1] = (server->port & 0x00FF);

    /* Fill out the destination IP address */
    for (i = 0; i < 4; i++) {
      server->addr.sa_data[i + 2] = 0;
      //server->addr.sa_data[i + 2] = connInfo.aucIP[3-i];
    }

    /* Set the rest of the dest_addr struct to 0 */
    for (i = 6; i < 14; i++) {
      server->addr.sa_data[i] = 0;
    }

    if ( bind(server->socket, &server->addr, sizeof(server->addr)) != 0 ) {
      printf("Failed to bind socket\n");
      return false;
    }

    if ( listen(server->socket, 1) != 0 ) {
      printf("Failed to listen on socket\n");
      return false;
    }
    return true;
}

void startListenServers(void)
{
   httpServer.port = 80;
   telnetServer.port = 23;

   startListenServer(&httpServer);
   startListenServer(&telnetServer);

   struct timeval timeout;
   timeout.tv_sec = 2;
   timeout.tv_usec = 0;
   socklen_t optlen = sizeof(timeout);

   int err = setsockopt(httpServer.socket, SOL_SOCKET, SOCKOPT_RECV_TIMEOUT, &timeout, optlen);
   if ( err == 0) {
  	 int sec = timeout.tv_sec;
  	 printf("Setting timeout to: %d s\n", sec);
   } else {
   	printf("setsockopt() returned error: %d\n", err);
   }

   err = setsockopt(telnetServer.socket, SOL_SOCKET, SOCKOPT_RECV_TIMEOUT, &timeout, optlen);
   if ( err == 0) {
  	 int sec = timeout.tv_sec;
  	 printf("Setting timeout to: %d s\n", sec);
   } else {
   	printf("setsockopt() returned error: %d\n", err);
   }

}

void handleQuery(int32_t socket, char *query)
{
  if ( !strcmp(query, "/") ) {
    queryRoot(socket);
  } else if ( !strcmp(query, "/users") ) {
    queryUsers(socket);
  } else if ( !strcmp(query, "/items") ) {
    queryItems(socket);
  } else if ( !strcmp(query, "/transactions") ) {
  	queryTransactions(socket);
  }
  else
  {
    send(socket, HTTP_HEADER_404, strlen(HTTP_HEADER_404), 0);
  }
}


void handleHttpClient(int32_t clientSocket, sockaddr clientAddr)
{
  int bytesRecvd;
  char query[MAX_QUERY_LEN];
  int n;

  uint32_t startTime = msTicks;

  printf("New HTTP connection from ");
  printIpAddr(&clientAddr.sa_data[0], true);
  printf("\n");


  memset(requestBuffer,0,sizeof(requestBuffer));
  bytesRecvd = recv(clientSocket, requestBuffer, sizeof(requestBuffer), 0);

  okToShutDown = false;

	n = sscanf(requestBuffer, "GET %s HTTP/1.1", query);
	if ( n <= 0 ) {
		send(clientSocket, HTTP_HEADER_400_BAD_REQ, strlen(HTTP_HEADER_400_BAD_REQ), 0);
	} else {
		handleQuery(clientSocket, query);
	}

  uint32_t msStart = msTicks;
  while (!okToShutDown && msTicks < msStart + 100);

  closesocket(clientSocket);
}


void reopenServer(Server *server)
{
  // BUG: Socket inactive so reopen socket
  // Inactive Socket, close and reopen it
  closesocket(server->socket);
  startListenServer(server);
  hci_unsolicited_event_handler();
}


void handleNetworkConnections(void)
{
  sockaddr clientAddr;
  socklen_t len;
  int32_t client;

  len = sizeof(sockaddr);
  client = accept(httpServer.socket, &clientAddr, &len);
  if ( client >= 0 ) {
    handleHttpClient(client, clientAddr);
  } else if ( client == -57 ) {
  	printf("HTTP socket closed. Reopening socket\n");
    reopenServer(&httpServer);
  }

  len = sizeof(sockaddr);
  client = accept(telnetServer.socket, &clientAddr, &len);
  if ( client >= 0 ) {
    telnetHandleClient(client, clientAddr);
  } else if ( client == -57 ) {
  	printf("Telnet socket closed. Reopening socket\n");
    reopenServer(&telnetServer);
  }
}

