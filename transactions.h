#ifndef _TRANSACTIONS_H_
#define _TRANSACTIONS_H_

#define MAX_NAME_LEN      40 // Should be divisible by 4
#define MAX_USERS         50
#define MAX_ITEMS         20
#define MAX_TRANSACTIONS 100

#define NO_ITEM -1

typedef struct {
  uint64_t cardId;
  char name[MAX_NAME_LEN];
  int32_t balance;
} User;

typedef struct {
  char name[MAX_NAME_LEN];
  int32_t price;
} Item;

typedef struct {
  int32_t user;
  int32_t item;
  int32_t value;
} Transaction;


typedef struct {
	User users[MAX_USERS];
	Item items[MAX_ITEMS];
	Transaction transactions[MAX_TRANSACTIONS];
	int32_t transactionStart;
	int32_t transactionEnd;
	int32_t numUsers;
	int32_t numItems;
	int32_t dbKey;
	int32_t crc;
} Database;


bool loadDatabase(void);

bool addUser(char *name, char *responseString);
bool editUser(int index, char *name, char *responseString);
int getUserCount(void);
int getUserIdByName(char *name);
int getUserIdByCardId(uint64_t cardId);
User getUserById(int userId);
bool creditUser(char *name, int credit, char *responseString);
int getNextUnregisteredUser(int userId);
int getPrevUnregisteredUser(int userId);
bool linkUser(int userId, uint64_t cardId, char *responseString);
int getUserIdByCardId(uint64_t cardId);

int getItemId(char *name);
bool addItem(char *name, int price, char *responseString);
bool editItem(int index, char *name, int price, char *responseString);
bool buyItem(int userId, int itemId, char *responseString);
Item getItem(int itemId);
int getItemCount(void);


#endif
