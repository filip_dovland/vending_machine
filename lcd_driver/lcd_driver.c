/*
 * lcdDriver.c
 *
 *  Created on: 28. juni 2015
 *      Author: maerlbec
 */

#include "em_gpio.h"
#include "em_cmu.h"
#include "lcd_driver.h"
#include "utils.h"
//#include "geckoAnimation.c"

void lcdCreateChar(uint8_t location, uint8_t charmap[]);


uint8_t silabs0_0[8] = {
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000001,
	0b00000011,
	0b00000011,
	0b00000110,
};

uint8_t silabs0_9[8] = {
	0b00001111,
	0b00001111,
	0b00001111,
	0b00001110,
	0b00001110,
	0b00001100,
	0b00000100,
	0b00000000,
};

uint8_t silabs6_0[8] = {
	0b00000000,
	0b00000010,
	0b00001000,
	0b00010000,
	0b00000001,
	0b00000011,
	0b00000011,
	0b00000011,
};

uint8_t silabs6_9[8] = {
	0b00000000,
	0b00000000,
	0b00011111,
	0b00001111,
	0b00011111,
	0b00011111,
	0b00000111,
	0b00000000,
};

uint8_t silabs12_0[8] = {
	0b00000000,
	0b00000000,
	0b00011111,
	0b00011111,
	0b00011111,
	0b00011111,
	0b00011111,
	0b00000000,
};

uint8_t silabs12_9[8] = {
	0b00000100,
	0b00000110,
	0b00011110,
	0b00011100,
	0b00011100,
	0b00011000,
	0b00000000,
	0b00000000,
};

uint8_t silabs18_0[8] = {
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000001,
	0b00000011,
	0b00000111,
	0b00011111,
	0b00001111,
};

uint8_t silabs18_9[8] = {
	0b00000110,
	0b00000110,
	0b00000100,
	0b00001000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
};

uint8_t gecko0_0[8] = {
	0b00000000,
	0b00001000,
	0b00001000,
	0b00001000,
	0b00001100,
	0b00001100,
	0b00001110,
	0b00000111,
};

uint8_t gecko0_9[8] = {
	0b00000011,
	0b00000001,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000011,
	0b00000011,
	0b00000011,
};

uint8_t gecko6_0[8] = {
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00001100,
	0b00001110,
	0b00001111,
	0b00000011,
};

uint8_t gecko6_9[8] = {
	0b00011111,
	0b00011111,
	0b00011111,
	0b00001111,
	0b00000111,
	0b00001110,
	0b00011110,
	0b00011100,
};

uint8_t gecko12_0[8] = {
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000000,
	0b00000010,
	0b00000110,
	0b00000111,
};

uint8_t gecko12_9[8] = {
	0b00000111,
	0b00011111,
	0b00011111,
	0b00011111,
	0b00011111,
	0b00011111,
	0b00000000,
	0b00000000,
};

uint8_t gecko18_0[8] = {
	0b00000000,
	0b00000000,
	0b00000010,
	0b00000111,
	0b00001111,
	0b00001111,
	0b00001111,
	0b00001111,
};

uint8_t gecko18_9[8] = {
	0b00011110,
	0b00011110,
	0b00011110,
	0b00011100,
	0b00011100,
	0b00000110,
	0b00000110,
	0b00000011,
};

uint8_t troll0_0[8] = {
	0b00000000,
	0b00000001,
	0b00000010,
	0b00000100,
	0b00000100,
	0b00001000,
	0b00010000,
	0b00010010,
};

uint8_t troll0_9[8] = {
	0b00010010,
	0b00001011,
	0b00001010,
	0b00001010,
	0b00001001,
	0b00001000,
	0b00001000,
	0b00000111,
};

uint8_t troll6_0[8] = {
	0b00000111,
	0b00011000,
	0b00000111,
	0b00011000,
	0b00000000,
	0b00011110,
	0b00010101,
	0b00011001,
};

uint8_t troll6_9[8] = {
	0b00000011,
	0b00000000,
	0b00011111,
	0b00001010,
	0b00011111,
	0b00000000,
	0b00000000,
	0b00011111,
};

uint8_t troll12_0[8] = {
	0b00011111,
	0b00000000,
	0b00011111,
	0b00000000,
	0b00000000,
	0b00011111,
	0b00010101,
	0b00001110,
};

uint8_t troll12_9[8] = {
	0b00000001,
	0b00001111,
	0b00010101,
	0b00001110,
	0b00011000,
	0b00000011,
	0b00001100,
	0b00010000,
};

uint8_t troll18_0[8] = {
	0b00011000,
	0b00000100,
	0b00010010,
	0b00001010,
	0b00000010,
	0b00000001,
	0b00001001,
	0b00011101,
};

uint8_t troll18_9[8] = {
	0b00010010,
	0b00000010,
	0b00000100,
	0b00001000,
	0b00010000,
	0b00000000,
	0b00000000,
	0b00000000,
};







uint8_t WENDING_MACHINE_LCD_FUNCTION = LCD_2LINE | LCD_4BITMODE | LCD_5x8DOTS;
uint8_t WENDING_MACHINE_LCD_DIPLAYCONTROL = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKON;
uint8_t WENDING_MACHINE_LCD_DISPLAYMODE = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;

extern volatile uint32_t msTicks; /* counts 1ms timeTicks */


uint8_t smiley[8] = { 0b00000, 0b10001, 0b00000, 0b00000, 0b10001, 0b01110, 0b00000, 0b00000};
uint8_t five[8] = {0b11111,0b11000,0b11110,0b11,0b11,0b11011,0b1110,0b0};



void loadSilabsLogo(void)
{
	lcdCreateChar(0,silabs0_0);
	lcdCreateChar(1,silabs0_9);
	lcdCreateChar(2,silabs6_0);
	lcdCreateChar(3,silabs6_9);
	lcdCreateChar(4,silabs12_0);
	lcdCreateChar(5,silabs12_9);
	lcdCreateChar(6,silabs18_0);
	lcdCreateChar(7,silabs18_9);
}

void loadGecko(void)
{
	lcdCreateChar(0,gecko0_0);
	lcdCreateChar(1,gecko0_9);
	lcdCreateChar(2,gecko6_0);
	lcdCreateChar(3,gecko6_9);
	lcdCreateChar(4,gecko12_0);
	lcdCreateChar(5,gecko12_9);
	lcdCreateChar(6,gecko18_0);
	lcdCreateChar(7,gecko18_9);
}


void loadTroll(void)
{
	lcdCreateChar(0,troll0_0);
	lcdCreateChar(1,troll0_9);
	lcdCreateChar(2,troll6_0);
	lcdCreateChar(3,troll6_9);
	lcdCreateChar(4,troll12_0);
	lcdCreateChar(5,troll12_9);
	lcdCreateChar(6,troll18_0);
	lcdCreateChar(7,troll18_9);
}


void lcdWriteCustom(unsigned int x, unsigned int y){
  lcdSetCursor(x,y);
  lcdWrite8BitValue(0, 1);
  lcdSetCursor(x,y+1);
  lcdWrite8BitValue(1, 1);
  lcdSetCursor(x+1,y);
  lcdWrite8BitValue(2, 1);
  lcdSetCursor(x+1,y+1);
  lcdWrite8BitValue(3, 1);
  lcdSetCursor(x+2,y);
  lcdWrite8BitValue(4, 1);
  lcdSetCursor(x+2,y+1);
  lcdWrite8BitValue(5, 1);
  lcdSetCursor(x+3,y);
  lcdWrite8BitValue(6, 1);
  lcdSetCursor(x+3,y+1);
  lcdWrite8BitValue(7, 1);
}


/*
 * Initialize pins used for LCD according to their setup
 * in the header file
 */
void lcdPinInit()
{
	GPIO_PinModeSet(LCD_DATA0_PORT, LCD_DATA0_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(LCD_DATA1_PORT, LCD_DATA1_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(LCD_DATA2_PORT, LCD_DATA2_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(LCD_DATA3_PORT, LCD_DATA3_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(LCD_RS_PORT, LCD_RS_PIN, gpioModePushPull, 0);
	//GPIO_PinModeSet(LCD_DATA3_PORT, LCD_DATA3_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(LCD_E_PORT, LCD_E_PIN, gpioModePushPull, 0);
}

void lcdGPIO_PinOutSetVal(GPIO_Port_TypeDef port, unsigned int pin, unsigned int value)
{
	if(value==0)
	{
		GPIO_PinOutClear(port, pin);
	}
	else
	{
		GPIO_PinOutSet(port, pin);
	}
}

/*
 * Write values to the 4 data pins
 * and pulse the enable pin
 */
void lcdWrite4DataBits(uint8_t dataValue)
{
	lcdGPIO_PinOutSetVal(LCD_DATA0_PORT, LCD_DATA0_PIN, (dataValue>>0) & 1);
	lcdGPIO_PinOutSetVal(LCD_DATA1_PORT, LCD_DATA1_PIN, (dataValue>>1) & 1);
	lcdGPIO_PinOutSetVal(LCD_DATA2_PORT, LCD_DATA2_PIN, (dataValue>>2) & 1);
	lcdGPIO_PinOutSetVal(LCD_DATA3_PORT, LCD_DATA3_PIN, (dataValue>>3) & 1);
	lcdPulseEnablePin();
}

void lcdPulseEnablePin(void)
{
    GPIO_PinOutSet(LCD_E_PORT, LCD_E_PIN);
    //for(volatile int i=0; i<100; i++);    // enable pulse must be >450ns
    delayUs(10 * 4);
    GPIO_PinOutClear(LCD_E_PORT, LCD_E_PIN);
    //for(volatile int i=0; i<10000; i++);   // commands need > 37us to settle
    delayUs(37 * 4);
}

void lcdWriteCommand(uint8_t command)
{
	lcdWrite8BitValue(command, 0);
}

void lcdWriteToScreen(uint8_t command)
{
	lcdWrite8BitValue(command, 1);
}

void lcdWriteArrayToScreen(const char *stringToWrite)
{
	//lcdWriteBitches(stringToWrite);
	while(*stringToWrite)
	{
		lcdWriteToScreen(*stringToWrite++);
	}
}

void lcdWriteArrayToCenterOfScreen(unsigned int row, const char *stringToWrite)
{
	//determine message length
	unsigned int startColumn = (WENDING_MACHINE_LCD_COLUMNS - strlen(stringToWrite))/2;
	lcdSetCursor(startColumn, row);
	lcdWriteArrayToScreen(stringToWrite);
}


/*
 * Set cursor to a given screen position.
 * Indexing starts in upper left corner.
 * Valid values:
 * 0 <= col <= 3
 * 0 <= row <= 19
 */
void lcdSetCursor(uint8_t col, uint8_t row)
{
  int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
  if ( row > WENDING_MACHINE_LCD_ROWS-1 ) {
    row = WENDING_MACHINE_LCD_ROWS-1;    // if row exceeds 4, place on lowest line
  }

  lcdWriteCommand(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

/*
 * Write a 8-bit value to either print register (mode=1),
 * or command register (mode=0) of LCD controller.
 * The 8-bit value is written as two 4-bit "nibbles",
 * sending the MSBs first, then LSBs
 */
void lcdWrite8BitValue(uint8_t dataValue, unsigned int mode)
{
	//set RS (registry select) according to mode
	if(mode==0)
	{
		GPIO_PinOutClear(LCD_RS_PORT, LCD_RS_PIN);
	}
	else
	{
		GPIO_PinOutSet(LCD_RS_PORT, LCD_RS_PIN);
	}

	//write first MSBs, then LSBs
	lcdWrite4DataBits(dataValue>>4);
	lcdWrite4DataBits(dataValue);
}

/*
 * Clears display and sends cursor to 0,0
 */
void lcdClearDisplay(void)
{
	lcdWriteCommand(LCD_CLEARDISPLAY);
	delayMs(2);
}

/*
 * Turns OFF underline cursor
 */
void lcdNoCursor(void) {
  WENDING_MACHINE_LCD_DIPLAYCONTROL &= ~LCD_CURSORON;
  lcdWriteCommand(LCD_DISPLAYCONTROL | WENDING_MACHINE_LCD_DIPLAYCONTROL);
}

/*
 * Turns ON underline cursor
 */
void lcdCursor() {
  WENDING_MACHINE_LCD_DIPLAYCONTROL |= LCD_CURSORON;
  lcdWriteCommand(LCD_DISPLAYCONTROL | WENDING_MACHINE_LCD_DIPLAYCONTROL);
}

/*
 * Sends cursor to 0,0 position
 */
void lcdHomeCursor(void)
{
	lcdWriteCommand(LCD_RETURNHOME);
	delayMs(2);
}

/*
 * Turn OFF blinker at cursor position
 */
void lcdNoBlink() {
  WENDING_MACHINE_LCD_DIPLAYCONTROL &= ~LCD_BLINKON;
  lcdWriteCommand(LCD_DISPLAYCONTROL | WENDING_MACHINE_LCD_DIPLAYCONTROL);
}


/*
 * Turn ON blinker at cursor position
 */
void lcdBlink() {
  WENDING_MACHINE_LCD_DIPLAYCONTROL |= LCD_BLINKON;
  lcdWriteCommand(LCD_DISPLAYCONTROL | WENDING_MACHINE_LCD_DIPLAYCONTROL);
}


/*
 * Switch display off (ie not show any content. The communication is still perfectly fine)
 */
void lcdNoDisplay() {
  WENDING_MACHINE_LCD_DIPLAYCONTROL &= ~LCD_DISPLAYON;
  lcdWriteCommand(LCD_DISPLAYCONTROL | WENDING_MACHINE_LCD_DIPLAYCONTROL);
}

/*
 * Switch display on (ie show content. This function is not the init function).
 */
void lcdDisplay() {
  WENDING_MACHINE_LCD_DIPLAYCONTROL |= LCD_DISPLAYON;
  lcdWriteCommand(LCD_DISPLAYCONTROL | WENDING_MACHINE_LCD_DIPLAYCONTROL);
}

/*
 * Scroll all text on the display 1 position to the left
 * NB! This will wrap text to other lines if end-of-line is reached
 */
void lcdScrollDisplayLeft(void) {
  lcdWriteCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

/*
 * Scroll all text on the display 1 position to the right
 * NB! This will wrap text to other lines if end-of-line is reached
 */
void lcdScrollDisplayRight(void) {
  lcdWriteCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

/*
 * Right-justifies text according to the cursor
 * NB! Pointer position seems to also move with this setup
 */
void lcdAutoscroll(void) {
  WENDING_MACHINE_LCD_DISPLAYMODE |= LCD_ENTRYSHIFTINCREMENT;
  lcdWriteCommand(LCD_ENTRYMODESET | WENDING_MACHINE_LCD_DISPLAYMODE);
}

/*
 * Left-justification according to the cursor
 */
void lcdNoAutoscroll(void) {
  WENDING_MACHINE_LCD_DISPLAYMODE &= ~LCD_ENTRYSHIFTINCREMENT;
  lcdWriteCommand(LCD_ENTRYMODESET | WENDING_MACHINE_LCD_DISPLAYMODE);
}


// Allows us to fill the first 8 CGRAM locations
// with custom characters
void lcdCreateChar(uint8_t location, uint8_t charmap[]) {
  location &= 0x7; // we only have 8 locations 0-7
  lcdWriteCommand(LCD_SETCGRAMADDR | (location << 3));
  for (int i=0; i<8; i++) {
    lcdWrite8BitValue(charmap[i], 1);
  }
}


/*
 * Initialize screen to 4-bit mode
 */
void lcdDriverInit(void)
{

    //Delay LCD for 50 ms before attempting to program it
	delayMs(50);

	//Initial mode is 8-bit mode. Following sequence is adopted from page 46 of datasheet
	//First write
	lcdWrite4DataBits(0x3);
	delayMs(5);

	//Second write
	lcdWrite4DataBits(0x3);
	delayMs(5);


	//Third write
	lcdWrite4DataBits(0x3);
	delayUs(150 * 4);

	//Alright, go to 4-bit mode
	lcdWrite4DataBits(0x2);

	//Set LCD function (number of lines, which mode, dot setting)
	lcdWriteCommand(LCD_FUNCTIONSET | WENDING_MACHINE_LCD_FUNCTION);
	lcdWriteCommand(LCD_DISPLAYCONTROL | WENDING_MACHINE_LCD_DIPLAYCONTROL);
	lcdClearDisplay();
	lcdWriteCommand(LCD_ENTRYMODESET | WENDING_MACHINE_LCD_DISPLAYMODE);

}/* lcd_init */

/*
 * Test function for LCD screen
 */
void lcdTestFunction(void)
{

  CMU_ClockEnable(cmuClock_GPIO, true);

  lcdPinInit();
  lcdDriverInit();

  char variable1[] = "     ROFL:ROFL:ROFL";
  char variable2[] = " L       ___^____  ";
  char variable3[] = "LOL=====/     [] \\ ";
  char variable4[] = " L      \\_________]";

  char avariable1[] = "            |      ";
  char avariable2[] = "L L";
  char avariable3[] = " O ";
  char avariable4[] = "L L";

  char bvariable1[] = "     ROFL:ROFL:ROFL";
  char bvariable2[] = " L ";
  char bvariable3[] = "LOL";
  char bvariable4[] = " L ";

/*
	lcdSetCursor(0,0);
	lcdWriteArrayToScreen(variable1);
	lcdSetCursor(0,1);
	lcdWriteArrayToScreen(variable2);
	lcdSetCursor(0,2);
	lcdWriteArrayToScreen(variable3);
	lcdSetCursor(0,3);
	lcdWriteArrayToScreen(variable4);



  while(1) {
  lcdSetCursor(0,0);
  lcdWriteArrayToScreen(avariable1);
  lcdSetCursor(0,1);
  lcdWriteArrayToScreen(avariable2);
  lcdSetCursor(0,2);
  lcdWriteArrayToScreen(avariable3);
  lcdSetCursor(0,3);
  lcdWriteArrayToScreen(avariable4);
  lcdSetCursor(0,0);
  lcdWriteArrayToScreen(bvariable1);
    lcdSetCursor(0,1);
    lcdWriteArrayToScreen(bvariable2);
    lcdSetCursor(0,2);
    lcdWriteArrayToScreen(bvariable3);
    lcdSetCursor(0,3);
    lcdWriteArrayToScreen(bvariable4);
  }

*/


  //lcdWriteArrayToScreen("Test");



  loadSilabsLogo();
  //loadGecko();
  //loadTroll();
  lcdHomeCursor();
  lcdNoBlink();
  lcdWriteCustom(0,1);


  while(1);
    while(1){
	  for(int i=0;i<16;i++)
	  {
		  lcdScrollDisplayRight();
		  delayMs(500);
	  }

	  for(int i=0;i<16;i++)
	  {
		  lcdScrollDisplayLeft();
		  delayMs(500);
	  }

  }


  //lcdWriteArrayToScreen("Welcome to the awesome Hack-A-Gecko project, the energy friendly wending machine, which will be the most awesome project ever seen");
  //lcdWriteArrayToCenterOfScreen(0, "EFMW");
  //lcdWriteArrayToCenterOfScreen(1, "Energy-friendly");
  //lcdWriteArrayToCenterOfScreen(2, "Wending-Machine");
  //lcdWriteArrayToCenterOfScreen(3, "How may I help you?");
}

void lcdPrint(char *string)
{
	lcdHomeCursor();
  lcdWriteArrayToScreen(string);
}

void lcdPrintAt(int row, int col, char *string)
{
	lcdSetCursor(col, row);
	lcdWriteArrayToScreen(string);
}

void initLcd(void)
{
  lcdPinInit();
  lcdDriverInit();
  lcdNoBlink();
  lcdHomeCursor();
}


