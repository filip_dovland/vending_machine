/*
 * lcd_driver.h
 *
 *  Created on: 28. juni 2015
 *      Author: maerlbec
 */

#ifndef LCDDRIVER_H_
#define LCDDRIVER_H_

#include "em_gpio.h"

void lcdPinInit();
void lcdGPIO_PinOutSetVal(GPIO_Port_TypeDef port, unsigned int pin, unsigned int value);
void lcdWrite4DataBits(uint8_t dataValue);
void lcdPulseEnablePin(void);
void lcdWriteCommand(uint8_t command);
void lcdWriteToScreen(uint8_t command);
void lcdSetCursor(uint8_t col, uint8_t row);
void lcdWrite8BitValue(uint8_t dataValue, unsigned int mode);
void lcdClearDisplay(void);
void lcdDriverInit(void);
void  lcdHomeCursor();
void  lcdNoCursor();
void  lcdCursor();
void  lcdNoBlink();
void  lcdBlink();
void  lcdNoDisplay();
void lcdDisplay();
void lcdTestFunction(void);
void lcdPrintAt(int row, int col, char *string);
void initLcd(void);
void lcdPrint(char *string);


//Set up data pins and relate them to correct data ports
//For now: data lines on same ports
#ifndef LCD_PORT
#define LCD_PORT         gpioPortD	/**< port for the LCD lines   */
#endif
#ifndef LCD_DATA0_PORT
#define LCD_DATA0_PORT   gpioPortD     /**< port for 4bit data bit 0 */
#endif
#ifndef LCD_DATA1_PORT
#define LCD_DATA1_PORT   gpioPortD     /**< port for 4bit data bit 1 */
#endif
#ifndef LCD_DATA2_PORT
#define LCD_DATA2_PORT   gpioPortD     /**< port for 4bit data bit 2 */
#endif
#ifndef LCD_DATA3_PORT
#define LCD_DATA3_PORT   gpioPortA     /**< port for 4bit data bit 3 */
#endif
#ifndef LCD_DATA0_PIN
#define LCD_DATA0_PIN    5            /**< pin for 4bit data bit 0  */
#endif
#ifndef LCD_DATA1_PIN
#define LCD_DATA1_PIN    4            /**< pin for 4bit data bit 1  */
#endif
#ifndef LCD_DATA2_PIN
#define LCD_DATA2_PIN    6            /**< pin for 4bit data bit 2  */
#endif
#ifndef LCD_DATA3_PIN
#define LCD_DATA3_PIN    14            /**< pin for 4bit data bit 3  */
#endif
#ifndef LCD_RS_PORT
#define LCD_RS_PORT      gpioPortD     /**< port for RS line         */
#endif
#ifndef LCD_RS_PIN
#define LCD_RS_PIN       7            /**< pin  for RS line         */
#endif
#ifndef LCD_E_PORT
#define LCD_E_PORT       gpioPortC     /**< port for Enable line     */
#endif
#ifndef LCD_E_PIN
#define LCD_E_PIN        4            /**< pin  for Enable line     */
#endif


//Delay constants
#ifndef LCD_DELAY_BOOTUP
#define LCD_DELAY_BOOTUP   40      /**< delay in micro seconds after power-on  */
#endif
#ifndef LCD_DELAY_INIT
#define LCD_DELAY_INIT      5      /**< delay in micro seconds after initialization command sent  */
#endif
#ifndef LCD_DELAY_INIT_REP
#define LCD_DELAY_INIT_REP    150      /**< delay in micro seconds after initialization command repeated */
#endif
#ifndef LCD_DELAY_INIT_4BIT
#define LCD_DELAY_INIT_4BIT   64      /**< delay in micro seconds after setting 4-bit mode */
#endif
#ifndef LCD_DELAY_BUSY_FLAG
#define LCD_DELAY_BUSY_FLAG    4      /**< time in micro seconds the address counter is updated after busy flag is cleared */
#endif
#ifndef LCD_DELAY_ENABLE_PULSE
#define LCD_DELAY_ENABLE_PULSE 1      /**< enable signal pulse width in micro seconds */
#endif


// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00



#define WENDING_MACHINE_LCD_COLUMNS 20
#define WENDING_MACHINE_LCD_ROWS 4


#endif /* LCDDRIVER_H_ */
