/******************************************************************************
 * @file rfid_read.h
 * @brief RFID Reader API
 * @author Abreham Emishaw
 * @version 1.00
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Labs has no
 * obligation to support this Software. Silicon Labs is providing the
 * Software "AS IS", with no express or implied warranties of any kind,
 * including, but not limited to, any implied warranties of merchantability
 * or fitness for any particular purpose or warranties against infringement
 * of any proprietary rights of a third party.
 *
 * Silicon Labs will not be liable for any consequential, incidental, or
 * special damages, or any other relief, or for any claim by any third party,
 * arising from your use of this Software.
 *
 ******************************************************************************/

#ifndef __RFID_H_
#define __RFID_H_

//#define DEBUG=1

/* Define termination character */
#define STX_CHAR    0x2
#define ETX_CHAR    0x3

/* Declare a circular buffer structure to use for Rx buffer  s*/
#define BUFFERSIZE  16

#define RFID_PACKAGE_SIZE 16

typedef struct {
	uint8_t stx;
	uint8_t data[10];
	uint8_t checksum[2];
	uint8_t cr;
	uint8_t lf;
	uint8_t etx;
} _RfidPackage;

typedef union {
	_RfidPackage package;
	uint8_t buffer[RFID_PACKAGE_SIZE];
} RfidPackage;


/******************************************************************************
 * @brief  uartSetup function
 *
 *****************************************************************************/
void uartSetup(void);

/******************************************************************************
 * @brief  cmuSetup function
 *
 *****************************************************************************/
void cmuSetup(void);

/******************************************************************************
 * @brief  rfIdReader function
 *
 *****************************************************************************/
void initRfid(void);
void handleRfid(void);

#endif


