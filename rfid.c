
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "segmentlcd.h"
#include "rfid.h"
#include "ui.h"
#include "transactions.h"

volatile struct Buffer
{
  uint8_t  data[BUFFERSIZE];  /* data buffer */
  uint32_t wrI;               /* write index */
} rxBuf = { {0}, 0};


static RfidPackage rfidValue;
static int rxIndex = 0;

static volatile bool rfidReceived = false;
static volatile uint64_t lastCardId;

extern volatile uint32_t msTicks;
static uint32_t lastByteTimeStamp;


/* Setup UART1 in async mode for RS232*/
static USART_TypeDef           * uart   = UART0;
static USART_InitAsync_TypeDef uartInit = USART_INITASYNC_DEFAULT;


/******************************************************************************
 * @brief  rfidReader function
 *
 *****************************************************************************/
void initRfid(void)
{
  /* Initialize clocks and oscillators */
  cmuSetup( );

  /* Initialize UART peripheral */
  uartSetup( );

  /* Enable LCD without voltage boost */
  //SegmentLCD_Init(false);
  
}

/******************************************************************************
 * @brief  uartSetup function
 *
 ******************************************************************************/
void uartSetup(void)
{
  /* Enable clock for GPIO module (required for pin configuration) */
  CMU_ClockEnable(cmuClock_GPIO, true);

  /* Configure GPIO pins */
  //GPIO_PinModeSet(gpioPortB, 9, gpioModePushPull, 1);
  GPIO_PinModeSet(RFID_RX_PORT, RFID_RX_PIN, gpioModeInput, 0);

  /* Prepare struct for initializing UART in asynchronous mode*/
  uartInit.enable       = usartDisable;   /* Don't enable UART upon initialization */
  uartInit.refFreq      = 0;              /* Provide information on reference frequency. When set to 0, the reference frequency is */
  uartInit.baudrate     = 9600;         /* Baud rate */
  uartInit.oversampling = usartOVS16;     /* Oversampling. Range is 4x, 6x, 8x or 16x */
  uartInit.databits     = usartDatabits8; /* Number of data bits. Range is 4 to 10 */
  uartInit.parity       = usartNoParity;  /* Parity mode */
  uartInit.stopbits     = usartStopbits1; /* Number of stop bits. Range is 0 to 2 */
  uartInit.mvdis        = false;          /* Disable majority voting */
  uartInit.prsRxEnable  = false;          /* Enable USART Rx via Peripheral Reflex System */
  uartInit.prsRxCh      = usartPrsRxCh0;  /* Select PRS channel if enabled */

  /* Initialize USART with uartInit struct */
  USART_InitAsync(uart, &uartInit);

  /* Prepare UART Rx and Tx interrupts */
  USART_IntClear(uart, _UART_IFC_MASK);
  USART_IntEnable(uart, UART_IEN_RXDATAV);
  NVIC_ClearPendingIRQ(UART0_RX_IRQn);
  NVIC_ClearPendingIRQ(UART0_TX_IRQn);
  NVIC_EnableIRQ(UART0_RX_IRQn);
  //NVIC_EnableIRQ(UART1_TX_IRQn);

  /* Enable I/O pins at UART1 location #2 */
  uart->ROUTE = UART_ROUTE_RXPEN | UART_ROUTE_LOCATION_LOC1;

  /* Enable UART */
  USART_Enable(uart, usartEnable);
}


/***************************************************************************//**
* @brief Set up Clock Management Unit
******************************************************************************/
void cmuSetup(void)
{
  /* Enable clock for HF peripherals */
  CMU_ClockEnable(cmuClock_HFPER, true);

  /* Enable clock for USART module */
  CMU_ClockEnable(cmuClock_UART0, true);
}


static void verifyPackage(void)
{
	int i;

	if ( rfidValue.package.stx != STX_CHAR )
		return;
	if ( rfidValue.package.cr != '\r' )
		return;
	if ( rfidValue.package.lf != '\n' )
		return;
	if ( rfidValue.package.etx != ETX_CHAR )
		return;

	uint8_t id[5];
	uint8_t tmp[3];
	tmp[2] = 0;

	for ( i=0; i<5; i++ ) {
		tmp[0] = rfidValue.package.data[i*2];
		tmp[1] = rfidValue.package.data[i*2+1];
		sscanf(tmp, "%02x", &id[i]);
	}

	uint8_t checksum;
	tmp[0] = rfidValue.package.data[i*2];
	tmp[1] = rfidValue.package.data[i*2+1];
	sscanf(tmp, "%02x", &checksum);

	uint8_t calcChecksum = id[0] ^ id[1];
	for ( i=2; i<5; i++ ) {
		calcChecksum ^= id[i];
	}

	if ( calcChecksum != checksum )
		return;

    lastCardId = 0;
    for ( i=0; i<5; i++ ) {
      lastCardId |= (uint64_t)id[i] << (i * 8);
    }
	rfidReceived = true;
}

void UART0_RX_IRQHandler(void)
{
  if (uart->IF & UART_IF_RXDATAV) {
  	if ( msTicks - lastByteTimeStamp > 1000 ) {
  		rxIndex = 0;
  	}
  	rfidValue.buffer[rxIndex] = uart->RXDATA;

  	if ( ++rxIndex >= RFID_PACKAGE_SIZE ) {
  		rxIndex = 0;
  		verifyPackage();
  	}

  	lastByteTimeStamp = msTicks;
  }
}

void handleRfid(void)
{
	if ( rfidReceived ) {
		rfidReceived = false;
		printf("Received RFID: ");
		int i;
		for ( i=0; i<10; i++ ) {
			printf("%c", rfidValue.package.data[i]);
		}
		printf("\n");

		uint32_t high = (lastCardId >> 32) & 0xFFFFFFFF;
		uint32_t low = lastCardId          & 0xFFFFFFFF;
		printf("Card ID: %02x%08x\n", high, low);

		int userId = getUserIdByCardId(lastCardId);
		if ( userId < 0 ) {
			uiSetNewUser(lastCardId);
		} else {
			uiSetUser(userId);
		}
	}
}




