#ifndef _TELNET_CMD_H_
#define _TELNET_CMD_H_

void telnetSend(int32_t socket, char *msg);
void telnetHandleClient(int32_t clientSocket, sockaddr clientAddr);

#endif