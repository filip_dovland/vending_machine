#ifndef _UI_H_
#define _UI_H_

void buttonIrqHandler(uint32_t flags);
void uiSetNewUser(uint64_t cardId);
void uiSetUser(int userId);
void buttonIrqHandler(uint32_t flags);
void initUi(void);
void setUiStatusReady(void);
void handleUi(void);
bool isAtHomeScreen();

#endif
