#include <stdbool.h>
#include <stdio.h>
#include "em_device.h"
#include "ui.h"
#include "config.h"
#include "transactions.h"
#include "netapp.h"
#include "lcdDriver.h"


#define STARTUP             1
#define HOME                2
#define WELCOME_USER        3
#define PRODUCT_SELECT      4
#define BUY_CONFIRMATION    5
#define NEW_USER            6
#define ERROR               7
#define INSUFFICIENT_FUNDS  8
#define NOT_LOGGED_IN       9
#define NETWORK_INFO        10
#define LOGGED_OUT          11
#define BOUGHT_ITEM         12

// Ms debounce time for buttons
#define BUTTON_DEBOUNCE     200

// Ms timeout before logging out from the bought item screen
#define BOUGHT_ITEM_TIMEOUT 5000


#define NO_USER -1

extern volatile uint32_t msTicks;

static int currentScreen;
static int currentUser;
static int currentProduct;
static int lastScreen;
static uint64_t currentCardId;

static bool buttonUp;
static bool buttonDown;
static bool buttonOk;
static bool buttonCancel;

static char lcdBuffer[80];
static char responseBuffer[80];
static char errorBuffer[80];

static volatile uint32_t buttonDebounce = 0;
static int homeCancelCount = 0;
static uint32_t boughtItemTimeout = 0;

extern bool wifiConnected;
extern tNetappIpconfigRetArgs connInfo;


void buttonIrqHandler(uint32_t flags)
{
  if ( flags & (1 << BUTTON_UP_PIN) ) {
  	GPIO->IFC = 1 << BUTTON_UP_PIN;
  	if ( !GPIO_PinInGet(BUTTON_UP_PORT, BUTTON_UP_PIN) && msTicks > buttonDebounce ) {
  		buttonUp = true;
  		buttonDebounce = msTicks + BUTTON_DEBOUNCE;
  	}
  }

  if ( flags & (1 << BUTTON_DOWN_PIN) ) {
  	GPIO->IFC = 1 << BUTTON_DOWN_PIN;
  	if ( !GPIO_PinInGet(BUTTON_DOWN_PORT, BUTTON_DOWN_PIN) && msTicks > buttonDebounce ) {
  		buttonDown = true;
  		buttonDebounce = msTicks + BUTTON_DEBOUNCE;
  	}
  }
  if ( flags & (1 << BUTTON_CANCEL_PIN) ) {
  	GPIO->IFC = 1 << BUTTON_CANCEL_PIN;
  	if ( !GPIO_PinInGet(BUTTON_CANCEL_PORT, BUTTON_CANCEL_PIN) && msTicks > buttonDebounce ) {
  		buttonCancel = true;
  		buttonDebounce = msTicks + BUTTON_DEBOUNCE;
  	}
  }
  if ( flags & (1 << BUTTON_OK_PIN) ) {
  	GPIO->IFC = 1 << BUTTON_OK_PIN;
  	if ( !GPIO_PinInGet(BUTTON_OK_PORT, BUTTON_OK_PIN) && msTicks > buttonDebounce ) {
  		buttonOk = true;
  		buttonDebounce = msTicks + BUTTON_DEBOUNCE;
  	}
  }
}
  

void initUi(void)
{
  currentScreen = STARTUP;
  currentUser = NO_USER;
  currentProduct = 0;
  lastScreen = -1;
  buttonDebounce = 0;
}

void setUiStatusReady(void)
{
  currentScreen = HOME;
}

void screenStartup(void)
{
	lcdClearDisplay();
  lcdPrint("Booting...");
}

void screenError(void)
{
	lcdClearDisplay();
  sprintf(lcdBuffer, "Error: %s", errorBuffer);
  lcdPrint(lcdBuffer);
}

void screenHome(void)
{
	lcdClearDisplay();
  lcdPrintAt(1,0, "       Gecko        ");
  lcdPrintAt(2,0, "  Vending Machine   ");
}

void screenWelcomeUser(void)
{
	lcdClearDisplay();
  User user = getUserById(currentUser);
  lcdPrintAt(0,0, "Welcome");
  sprintf(lcdBuffer, "  %s", user.name);
  lcdPrintAt(1,0, lcdBuffer);
  sprintf(lcdBuffer, "Balance: %8d kr", user.balance);
  lcdPrintAt(3,0, lcdBuffer);
}

void screenProductSelection(void)
{
	char tmpBuf[20];
	lcdClearDisplay();
  Item item = getItem(currentProduct);
  if ( currentUser >= 0 ) {
  	User user = getUserById(currentUser);
  	sprintf(lcdBuffer, "%20s", user.name);
  	lcdPrintAt(0,0, lcdBuffer);
  }
  sprintf(lcdBuffer, "%s", item.name);
  lcdPrintAt(1,0, lcdBuffer);
  sprintf(tmpBuf, "Price: %3d kr", item.price);
  sprintf(lcdBuffer, "%20s", tmpBuf);
  lcdPrintAt(3,0, lcdBuffer);
}


void screenBuyConfirmation(void)
{
  Item item = getItem(currentProduct);
  lcdClearDisplay();
  sprintf(lcdBuffer, "Buying %s", item.name);
  lcdPrintAt(0,0, lcdBuffer);
  sprintf(lcdBuffer, "Price %d kr", item.price);
  lcdPrintAt(2,0, lcdBuffer);
  sprintf(lcdBuffer, "%20s", "OK?");
  lcdPrintAt(3,0, lcdBuffer);
}

void screenInsufficientFunds(void)
{
  lcdClearDisplay();
  lcdPrintAt(1,0, " Not enough gryn!");
}

void screenNewUser(void)
{
	lcdClearDisplay();
  if ( currentUser < 0 ) {
    lcdPrintAt(0, 0, "No unregistered");
    lcdPrintAt(1, 0, "users found");
  } else {
    User user = getUserById(currentUser);
    lcdPrintAt(0, 0, "Register RFID");
    sprintf(lcdBuffer, "User: %s", user.name);
    lcdPrintAt(2, 0, lcdBuffer);
  }
}

void screenNotLoggedIn(void)
{
	lcdClearDisplay();
  lcdPrintAt(1, 0, "   Not logged in!");
}

void screenLoggedOut(void)
{
	lcdClearDisplay();
  lcdPrintAt(1, 0, "     Logged out!    ");
}

void screenBoughtItem(void)
{
	Item item = getItem(currentProduct);
	User user = getUserById(currentUser);
	lcdClearDisplay();
  sprintf(lcdBuffer, "Bought %s", item.name);
  lcdPrintAt(0,0, lcdBuffer);
  sprintf(lcdBuffer, "for %d kr", item.price);
  lcdPrintAt(1,0, lcdBuffer);
  lcdPrintAt(2,0, "New balance is ");
  sprintf(lcdBuffer, "%d kr", user.balance);
  lcdPrintAt(3,0, lcdBuffer);

}

void screenNetworkInfo(void)
{
	lcdClearDisplay();
  if (wifiConnected) {
  	lcdPrintAt(0,0, "WiFi: Connected");
  	sprintf(lcdBuffer, "IP: %d.%d.%d.%d",connInfo.aucIP[3],connInfo.aucIP[2],connInfo.aucIP[1],connInfo.aucIP[0]);
  	lcdPrintAt(1,0, lcdBuffer);
  } else {
  	lcdPrintAt(0,0, "WiFi: Disconnected");
  }
}

void updateScreen(int screen)
{
  switch (screen) {
  case STARTUP:
  	screenStartup();
  	break;
  case HOME:
    screenHome();
    break;
  case WELCOME_USER:
    screenWelcomeUser();
    break;
  case PRODUCT_SELECT:
    screenProductSelection();
    break;
  case BUY_CONFIRMATION:
    screenBuyConfirmation();
    break;
  case INSUFFICIENT_FUNDS:
  	screenInsufficientFunds();
  	break;
  case NEW_USER:
    screenNewUser();
    break;
  case ERROR:
    screenError();
    break;
  case NOT_LOGGED_IN:
  	screenNotLoggedIn();
  	break;
  case NETWORK_INFO:
  	screenNetworkInfo();
  	break;
  case LOGGED_OUT:
  	screenLoggedOut();
  	break;
  case BOUGHT_ITEM:
  	screenBoughtItem();
  	break;
  default:
    sprintf(errorBuffer, "Unknown screen: %d", screen);
    screenError();
    break;
  }
}

void nextScreen(int screen)
{

	if (buttonUp)
		printf("Up\n");
	if (buttonDown)
		printf("Down\n");
	if (buttonCancel)
		printf("Cancel\n");
	if (buttonOk)
		printf("OK\n");

	if ( screen != HOME ) {
		homeCancelCount = 0;
	}

  switch (screen) {
  case STARTUP:
  	break;
  case HOME:
    if ( buttonDown ) {
      buttonDown = false;
      currentProduct = 0;
      currentScreen = PRODUCT_SELECT;
      updateScreen(currentScreen);
    }
    if ( buttonUp ) {
      buttonUp = false;
      currentProduct = getItemCount() - 1;
      currentScreen = PRODUCT_SELECT;
      updateScreen(currentScreen);
    }
    if ( buttonCancel ) {
    	buttonCancel = false;
    	if ( ++homeCancelCount >= 3 ) {
    		currentScreen = NETWORK_INFO;
    		updateScreen(currentScreen);
    	}
    }
    break;
  case WELCOME_USER:
    if ( buttonDown ) {
      buttonDown = false;
      currentProduct = 0;
      currentScreen = PRODUCT_SELECT;
      updateScreen(currentScreen);
    }
    if ( buttonUp ) {
      buttonUp = false;
      currentProduct = getItemCount() - 1;
      currentScreen = PRODUCT_SELECT;
      updateScreen(currentScreen);
    }
    if ( buttonCancel ) {
    	buttonCancel = false;
    	currentScreen = LOGGED_OUT;
    	currentUser = NO_USER;
    	updateScreen(currentScreen);
    }
    break;
  case PRODUCT_SELECT:
    if ( buttonOk ) {
      buttonOk = false;
      if ( currentUser < 0 ) {
      	currentScreen = NOT_LOGGED_IN;
      } else {
      	currentScreen = BUY_CONFIRMATION;
      }
      updateScreen(currentScreen);
    }
    if ( buttonDown ) {
      buttonDown = false;
      currentProduct = (currentProduct + 1) % getItemCount();
      updateScreen(currentScreen);
    }
    if ( buttonUp ) {
      buttonUp = false;
      currentProduct = currentProduct - 1;
      if ( currentProduct < 0 ) {
        currentProduct = getItemCount() - 1;
      }
      updateScreen(currentScreen);
    }
    if ( buttonCancel ) {
      buttonCancel = false;
      if ( currentUser != NO_USER ) {
      	currentScreen = LOGGED_OUT;
      	currentUser = NO_USER;
      } else {
      	currentScreen = HOME;
      }
      updateScreen(currentScreen);
    }
    break;
  case BUY_CONFIRMATION:
    if ( buttonOk ) {
      buttonOk = false;

      Item item = getItem(currentProduct);
      User user = getUserById(currentUser);

      if (item.price > user.balance) {
      	currentScreen = INSUFFICIENT_FUNDS;
      } else {
				bool status = buyItem(currentUser, currentProduct, responseBuffer);
				if ( !status ) {
					printf("Buying item failed: %s", responseBuffer);
					sprintf(errorBuffer, "Buying item failed: %s", responseBuffer);
					currentScreen = ERROR;
				} else {
					currentScreen = BOUGHT_ITEM;
					boughtItemTimeout = msTicks + BOUGHT_ITEM_TIMEOUT;
				}
      }
      updateScreen(currentScreen);
    }
    if ( buttonCancel ) {
      buttonCancel = false;
      currentScreen = PRODUCT_SELECT;
      updateScreen(currentScreen);
    }
    break;
  case INSUFFICIENT_FUNDS:
  	if ( buttonCancel ) {
  		buttonCancel = false;
  		currentScreen = PRODUCT_SELECT;
  		updateScreen(currentScreen);
  	}
  	if ( buttonOk ) {
  		buttonOk = false;
  		currentScreen = WELCOME_USER;
  		updateScreen(currentScreen);
  	}
  	break;
  case NEW_USER:
    if ( buttonUp ) {
      buttonUp = false;
      currentUser = getPrevUnregisteredUser(currentUser);
      updateScreen(currentScreen);
    }
    if ( buttonDown ) {
      buttonDown = false;
      currentUser = getNextUnregisteredUser(currentUser);
      updateScreen(currentScreen);
    }
    if ( buttonOk ) {
      buttonOk = false;
      bool status = linkUser(currentUser, currentCardId, responseBuffer);
      if ( !status ) {
        sprintf(errorBuffer,"Failed to link user: %s", responseBuffer);
        printf(errorBuffer);
        currentScreen = ERROR;
      } else {
        currentScreen = WELCOME_USER;
      }
      updateScreen(currentScreen);
    }
    if ( buttonCancel ) {
      buttonCancel = false;
      currentScreen = HOME;
      currentUser = NO_USER;
      updateScreen(currentScreen);
    }
    break;

  case ERROR:
    if ( buttonCancel ) {
      buttonCancel = false;
      currentScreen = HOME;
      currentUser = NO_USER;
      updateScreen(currentScreen);
    }
    break;
  case NETWORK_INFO:
  	if ( buttonOk ) {
  		buttonOk = false;
  		currentScreen = HOME;
  		updateScreen(currentScreen);
  	}
  	break;
  case LOGGED_OUT:
  	if ( buttonOk || buttonCancel || buttonUp || buttonDown ) {
  		currentScreen = HOME;
  		updateScreen(currentScreen);
  	}
  	break;
  case BOUGHT_ITEM:
  	if ( buttonOk || buttonCancel || buttonUp || buttonDown || msTicks > boughtItemTimeout ) {
			currentScreen = HOME;
			currentUser = NO_USER;
			updateScreen(currentScreen);
  	}
  	break;
  default:
    if ( buttonCancel ) {
      buttonCancel = false;
      currentScreen = HOME;
      currentUser = NO_USER;
      updateScreen(currentScreen);
    }
    break;
  }

  // Reset any button we didn't handle above. 
  buttonOk = false;
  buttonCancel = false;
  buttonUp = false;
  buttonDown = false;
}

void handleUi(void)
{
	nextScreen(currentScreen);
	if ( lastScreen != currentScreen ) {
		updateScreen(currentScreen);
	}
	lastScreen = currentScreen;
}

void uiSetUser(int userId)
{
  currentScreen = WELCOME_USER;
  currentUser = userId;
  updateScreen(currentScreen);
}

void uiSetNewUser(uint64_t cardId)
{
  currentScreen = NEW_USER;
  currentUser = getNextUnregisteredUser(0);
  currentCardId = cardId;
  updateScreen(currentScreen);
}

bool isAtHomeScreen() {
	return currentScreen == HOME;
}
