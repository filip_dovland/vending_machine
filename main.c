#include <stdio.h>
#include "em_device.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"
#include "wlan.h"
#include "hci.h"
#include "wifi_spi.h"
#include "utils.h"
#include "common.h"
#include "swo.h"
#include "socket.h"
#include "wifi_control.h"
#include "lcdDriver.h"
#include "transactions.h"
#include "rfid.h"
#include "ui.h"
#include "config.h"

volatile uint32_t msTicks = 0;

bool wifiConnected = false;

void SysTick_Handler(void)
{
	msTicks++;
}

void GPIO_IRQHandler(void)
{
  uint32_t flags = GPIO->IF & GPIO->IEN;

  if ( flags & (1 << WIFI_INT_PIN) ) {
    wifiIrqHandler();
  } else {
  	buttonIrqHandler(flags);
  }
  
}

void GPIO_EVEN_IRQHandler(void)
{
  GPIO_IRQHandler();
}

void GPIO_ODD_IRQHandler(void)
{
  GPIO_IRQHandler();
}

void HardFault_Handler(void)
{
	while(1);
}

void initButtons(void)
{
	GPIO_PinModeSet(BUTTON_UP_PORT, BUTTON_UP_PIN, gpioModeInputPull, 1);
	GPIO_PinModeSet(BUTTON_DOWN_PORT, BUTTON_DOWN_PIN, gpioModeInputPull, 1);
	GPIO_PinModeSet(BUTTON_CANCEL_PORT, BUTTON_CANCEL_PIN, gpioModeInputPull, 1);
	GPIO_PinModeSet(BUTTON_OK_PORT, BUTTON_OK_PIN, gpioModeInputPull, 1);

	GPIO_IntConfig(BUTTON_UP_PORT, BUTTON_UP_PIN,         false, true, true);
	GPIO_IntConfig(BUTTON_DOWN_PORT, BUTTON_DOWN_PIN,     false, true, true);
	GPIO_IntConfig(BUTTON_CANCEL_PORT, BUTTON_CANCEL_PIN, false, true, true);
	GPIO_IntConfig(BUTTON_OK_PORT, BUTTON_OK_PIN,         false, true, true);

	NVIC_SetPriority(GPIO_ODD_IRQn, 2);
	NVIC_SetPriority(GPIO_EVEN_IRQn, 2);

	NVIC_EnableIRQ(GPIO_ODD_IRQn);
	NVIC_EnableIRQ(GPIO_EVEN_IRQn);

}


void tryToConnectToWifi(void)
{
	if ( wifiConnected )
		return;

  lcdClearDisplay();
  lcdPrint("WiFi connect...");

  initWifi();
  wifiConnected = false;
  int retry = 5;
  while ( !wifiConnected && retry >= 0 ) {
  	wifiConnected = startWifi();
  	retry--;
  }

  startListenServers();
  setUiStatusReady();
}

int main(void)
{
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFRCO);
  SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000);
  
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_USART0, true);

  setupSWOForPrint();

  printf("Loading database\n");
  if ( !loadDatabase() ) {
  	printf("Failed to load database\n");
  	while(1);
  }

  initButtons();
  initRfid();
  initDelay();
  initLcd();
  initUi();

  tryToConnectToWifi();

  while(1)
  {
    handleNetworkConnections();
  	handleUi();
    handleRfid();
  }
}
