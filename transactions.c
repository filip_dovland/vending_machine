#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "em_device.h"
#include "em_msc.h"
#include "transactions.h"


// How many pages to use to store entire database
#define DATABASE_PAGES 2

#define DB_STORAGE_A (FLASH_SIZE - (DATABASE_PAGES * FLASH_PAGE_SIZE) * 1)
#define DB_STORAGE_B (FLASH_SIZE - (DATABASE_PAGES * FLASH_PAGE_SIZE) * 2)

Database database;

// TODO error reporting to telnet?

/* From http://rosettacode.org/wiki/CRC-32#C */
uint32_t rc_crc32(uint32_t crc, const char *buf, size_t len)
{
	static uint32_t table[256];
	static int have_table = 0;
	uint32_t rem;
	uint8_t octet;
	int i, j;
	const char *p, *q;

	/* This check is not thread safe; there is no mutex. */
	if (have_table == 0) {
		/* Calculate CRC table. */
		for (i = 0; i < 256; i++) {
			rem = i;  /* remainder from polynomial division */
			for (j = 0; j < 8; j++) {
				if (rem & 1) {
					rem >>= 1;
					rem ^= 0xedb88320;
				} else
					rem >>= 1;
			}
			table[i] = rem;
		}
		have_table = 1;
	}

	crc = ~crc;
	q = buf + len;
	for (p = buf; p < q; p++) {
		octet = *p;  /* Cast to unsigned octet. */
		crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
	}
	return ~crc;
}


uint32_t calcDbCrc(Database *db)
{
	return rc_crc32(0, (const char *)db, sizeof(Database)-4);
}

bool verifyDatabase(Database *db)
{
	return calcDbCrc(db) == db->crc;
}


bool storeDatabase(Database *dbStorage)
{
	uint32_t *page;
	int i;

	MSC_Init();

	page = (uint32_t *)dbStorage;

	for ( i=0; i<DATABASE_PAGES; i++ ) {
		MSC_ErasePage(page);
		page += FLASH_PAGE_SIZE / 4;
	}

	MSC_WriteWord((uint32_t *)dbStorage, &database, sizeof(Database));

	MSC_Deinit();

	return true;
}

bool saveDatabase(void)
{
	database.dbKey++;
	database.crc = calcDbCrc(&database);

	Database *dba = (Database *)DB_STORAGE_A;
	Database *dbb = (Database *)DB_STORAGE_B;

	bool dbaOK = verifyDatabase(dba);
	bool dbbOK = verifyDatabase(dbb);

	// Store in any invalid location first. Then choose the oldest
	if ( !dbaOK && !dbbOK ) {
		storeDatabase(dba);
	} else if ( dbaOK && !dbbOK )  {
		storeDatabase(dbb);
	} else if ( !dbaOK && dbbOK ) {
		storeDatabase(dba);
	} else if ( dba->dbKey > dbb->dbKey ) {
		storeDatabase(dbb);
	} else {
		storeDatabase(dba);
	}

  return true;
}

bool loadDatabase(void)
{
	if ( sizeof(Database) > FLASH_PAGE_SIZE * DATABASE_PAGES ) {
		printf("Error: flash area is too small to store database!\n");
		return false;
	}

	Database *dba = (Database *)DB_STORAGE_A;
	Database *dbb = (Database *)DB_STORAGE_B;
	Database *selDb = NULL;

	bool dbaOK = verifyDatabase(dba);
	bool dbbOK = verifyDatabase(dbb);

	// Store in any invalid location first. Then choose the oldest
	if ( !dbaOK && !dbbOK ) {
		// New db
	} else if ( dbaOK && !dbbOK )  {
		selDb = dba;
	} else if ( !dbaOK && dbbOK ) {
		selDb = dbb;
	} else if ( dba->dbKey > dbb->dbKey ) {
		selDb = dba;
	} else {
		selDb = dbb;
	}

	if ( selDb != NULL ) {
		memcpy(&database, selDb, sizeof(Database));
	} else {
		printf("No stored DB found. Creating new.\n");
		memset(&database, 0, sizeof(Database));
	}

  return true;
}

int getUserIdByName(char *name)
{
	int i;
	for ( i=0; i<database.numUsers; i++ ) {
		if ( !strcmp(name, database.users[i].name) ) {
			return i;
		}
	}
	return -1;
}

int getUserIdByCardId(uint64_t cardId)
{
	int i;
	for ( i=0; i<database.numUsers; i++ ) {
		if ( database.users[i].cardId == cardId) {
			return i;
		}
	}
	return -1;
}

int getItemId(char *name)
{
	int i;
	for ( i=0; i<database.numItems; i++ ) {
		if ( !strcmp(name, database.items[i].name) ) {
			return i;
		}
	}
	return -1;
}

/* Adds a transaction to the transaction log and updates the
 * useres balance. A transaction must involve a user, but the item is optional
 * (as when adding credit to a user).
 *
 * Note that this function does not check that the user has sufficient
 * funds, so the balance can become negative. The caller should check
 * this beforehand.
 *
 * @param userId Must be a valid user id
 * @param itemId Either a valid item id or -1 to indicate no item
 * @param amount The balance of the user will be adjusted by this amount.
 */
bool addTransaction(int userId, int itemId, int amount)
{
	if ( userId < 0 || userId >= database.numUsers ) {
		printf("addTransaction(): invalid user id: %d\n", userId);
		return false;
	}

	if ( itemId >= database.numItems ) {
		printf("addTransaction(): invalid item id: %d\n", itemId);
		return false;
	}

	// Transaction accepted

	database.transactions[database.transactionEnd].user = userId;
	database.transactions[database.transactionEnd].item = itemId;
	database.transactions[database.transactionEnd].value = amount;
	if ( ++database.transactionEnd >= MAX_TRANSACTIONS ) {
		database.transactionEnd = 0;
	}
	if ( database.transactionEnd == database.transactionStart ) {
		if ( ++database.transactionStart >= MAX_TRANSACTIONS ) {
			database.transactionStart = 0;
		}
	}

	database.users[userId].balance += amount;
	return true;
}


bool addUser(char *name, char *responseString)
{
  if ( database.numUsers >= MAX_USERS ) {
    sprintf(responseString, "Maximum limit of users reached\n");
    return false;
  }

  if ( strlen(name) > MAX_NAME_LEN ) {
    sprintf(responseString,"Name is too long\n");
    return false;
  }

  // Check if username already exsists
  int i;
  bool exists = false;
  for ( i=0; i<database.numUsers; i++ ) {
    if ( !strcmp(name, database.users[i].name) ) {
      exists = true;
      break;
    }
  }
  if ( exists ) {
    sprintf(responseString, "A user with that name already exists");
    return false;
  }

  // Add to database
  strcpy(database.users[database.numUsers].name, name);
  database.numUsers++;

  if ( saveDatabase() ) {
    sprintf(responseString, "Adding user %s with id %lu\n", name, database.numUsers-1);
    return true;
  } else {
    sprintf(responseString, "Failed saving database");
    return false;
  }
}


bool editUser(int index, char *name, char *responseString)
{
  if ( index < 0 || index >= database.numUsers ) {
    sprintf(responseString, "Invalid user index: %d\n", index);
    return false;
  }

  if ( strlen(name) > MAX_NAME_LEN ) {
    sprintf(responseString,"Name is too long\n");
    return false;
  }

  // Check if username already exsists
  int i;
  bool exists = false;
  for ( i=0; i<database.numUsers; i++ ) {
    if ( !strcmp(name, database.users[i].name) ) {
      exists = true;
      break;
    }
  }
  if ( exists ) {
    sprintf(responseString, "A user with that name already exists");
    return false;
  }

  // Add to database
  strcpy(database.users[index].name, name);

  if ( saveDatabase() ) {
    sprintf(responseString, "Editing user %s with id %lu\n", name, index);
    return true;
  } else {
    sprintf(responseString, "Failed saving database");
    return false;
  }
}

bool addItem(char *name, int price, char *responseString)
{
  if ( database.numItems >= MAX_ITEMS ) {
    sprintf(responseString, "Maximum number of items reached");
    return false;
  }

  if ( strlen(name) > MAX_NAME_LEN ) {
    sprintf(responseString, "Name is too long. Max length is %d.", MAX_NAME_LEN);
    return false;
  }

  // Add to database
  strcpy(database.items[database.numItems].name, name);
  database.items[database.numItems].price = price;
  database.numItems++;

  if ( saveDatabase() ) {
    sprintf(responseString, "Adding item %s with id %lu", name, database.numItems-1);
    return true;
  } else {
    sprintf(responseString, "Failed saving database");
    return false;
  }
}


bool editItem(int index, char *name, int price, char *responseString)
{
	if (index < 0 || index >= database.numItems ) {
		sprintf(responseString, "Invalid item index: %d", index);
		return false;
	}

  if ( strlen(name) > MAX_NAME_LEN ) {
    sprintf(responseString, "Name is too long. Max length is %d.", MAX_NAME_LEN);
    return false;
  }

  // Add to database
  strcpy(database.items[index].name, name);
  database.items[index].price = price;

  if ( saveDatabase() ) {
    sprintf(responseString, "Edited item %s with id %lu", name, index);
    return true;
  } else {
    sprintf(responseString, "Failed saving database");
    return false;
  }
}

bool creditUser(char *name, int credit, char *responseString)
{
  int i;
  int userId = -1;
  for ( i=0; i<database.numUsers; i++ ) {
    if ( !strcmp(name, database.users[i].name) ) {
      userId = i;
      break;
    }
  }

  if ( userId == -1 ) {
    sprintf(responseString, "A user with that name could not be found");
    return false;
  }

  // Update database
  if ( !addTransaction(userId, NO_ITEM, credit) ) {
  	sprintf(responseString, "Failed to add transaction");
  	return false;
  }

  if ( saveDatabase() ) {
    sprintf(responseString, "Credited %s with %d. New balance is %d",
        database.users[userId].name,
        credit,
        database.users[userId].balance);
    return true;
  } else {
    sprintf(responseString, "Failed saving database");
    return false;
  }
}

bool buyItem(int userId, int itemId, char *responseString)
{
	if ( userId < 0 || userId > database.numUsers ) {
		sprintf(responseString, "Invalid user ID %d", userId);
		return false;
	}

	if ( itemId < 0 || itemId > database.numItems ) {
		sprintf(responseString, "Invalid item ID %d", itemId);
		return false;
	}

	// No credit!
	if ( database.users[userId].balance - database.items[itemId].price < 0 ) {
		sprintf(responseString, "Insufficient funds");
		return false;
	}

	// Do transaction
	if ( !addTransaction(userId, itemId, -database.items[itemId].price) ) {
  	sprintf(responseString, "Failed to add transaction");
  	return false;
  }

  if ( saveDatabase() ) {
    sprintf(responseString, "%s bought %s for %d with card 0x%lx. New balance is %d.",
    		database.users[userId].name,
    		database.items[itemId].name,
    		database.items[itemId].price,
    		database.users[userId].cardId,
    		database.users[userId].balance);
    return true;
  } else {
    sprintf(responseString, "Failed saving database");
    return false;
  }

}

Item getItem(int itemId)
{
  return database.items[itemId];
}

int getItemCount(void)
{
  return database.numItems;
}

int getUserCount(void)
{
	return database.numUsers;
}

int getNextUnregisteredUser(int userId)
{
  int index = userId;
  do {
    index = (index + 1) % database.numUsers;
    if ( database.users[index].cardId == 0 ) {
      return index;
    }
  } while ( index != userId );

  return -1;
}
  
int getPrevUnregisteredUser(int userId)
{
  int index = userId;
  do {
    index = index - 1;
    if ( index < 0 ) {
      index = database.numUsers - 1;
    }
    if ( database.users[index].cardId == 0 ) {
      return index;
    }
  } while ( index != userId );

  return -1;
}

bool linkUser(int userId, uint64_t cardId, char *responseString)
{
  if ( userId >= database.numUsers ) {
    sprintf(responseString, "Invalid user ID: %d", userId);
    return false;
  }

  if ( cardId == 0 ) {
    sprintf(responseString, "Invalid card ID: %d", cardId);
    return false;
  }

  if ( database.users[userId].cardId != 0 ) {
    sprintf(responseString, "User %d is already registered", userId);
    return false;
  }

  database.users[userId].cardId = cardId;
  if ( saveDatabase() ) {
    sprintf(responseString, "Linking %s with card %lx\n", database.users[userId].name, cardId);
    return true;
  } else {
    sprintf(responseString, "Failed saving database");
    return false;
  }
}
    
User getUserById(int userId)
{
  return database.users[userId];
}
