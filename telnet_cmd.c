#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "socket.h"
#include "wlan.h"
#include "em_device.h"
#include "telnet_cmd.h"
#include "transactions.h"
#include "wifi_control.h"

#define TELNET_MAX_CMD_LEN    40

#define BUFFER_LENGTH 1024



static char responseBuffer[BUFFER_LENGTH];
static char tempBuffer[BUFFER_LENGTH];
static char telnetRequestBuffer[BUFFER_LENGTH];

extern volatile bool okToShutDown;
extern volatile uint32_t msTicks;

void telnetSend(int32_t socket, char *msg)
{
  send(socket, msg, strlen(msg), 0);
}

static bool telnetAddUser(int32_t clientSocket, int numArgs, char *name, int initialBalance)
{
  if ( numArgs < 1 ) {
    telnetSend(clientSocket, "You must specify a user name\n");
    return false;
  }

  // If balance is not specified, set to zero
  if ( numArgs < 2 ) {
    initialBalance = 0;
  }

  if ( addUser(name, tempBuffer) ) {
    sprintf(responseBuffer, "%s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return true;
  } else {
    sprintf(responseBuffer, "Error: %s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return false;
  }
}

static bool telnetEditUser(int32_t clientSocket, int numArgs, int index, char *name)
{
  if ( numArgs < 2 ) {
    telnetSend(clientSocket, "You must specify an index and a user name\n");
    return false;
  }

  if ( editUser(index, name, tempBuffer) ) {
    sprintf(responseBuffer, "%s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return true;
  } else {
    sprintf(responseBuffer, "Error: %s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return false;
  }
}

static bool telnetListUsers(int32_t clientSocket)
{
  int num = getUserCount();

  for ( int i=0; i<num; i++ ) {
  	User user = getUserById(i);
  	uint32_t cardHigh = (user.cardId >> 32) & 0xFFFFFFFF;
  	uint32_t cardLow = user.cardId & 0xFFFFFFFF;
    sprintf(responseBuffer, "%2d %-20s %3d kr %02x%08x\n", i, user.name, user.balance, cardHigh, cardLow);
    telnetSend(clientSocket, responseBuffer);
  }
  return true;
}

bool telnetAddItem(int32_t clientSocket, int numArgs, char *name, int price)
{
  if ( numArgs < 2 ) {
    telnetSend(clientSocket, "Error: You must specify both name and price for the item\n");
    return false;
  }

  if ( addItem(name, price, tempBuffer) ) {
    sprintf(responseBuffer, "%s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return true;
  } else {
    sprintf(responseBuffer, "Error: %s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return false;
  }
}

bool telnetEditItem(int32_t clientSocket, int numArgs, int index, char *name, int price)
{
  if ( numArgs < 3 ) {
    telnetSend(clientSocket, "Error: You must specify both id, name and price for the item\n");
    return false;
  }

  if ( editItem(index, name, price, tempBuffer) ) {
    sprintf(responseBuffer, "%s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return true;
  } else {
    sprintf(responseBuffer, "Error: %s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return false;
  }
}

bool telnetListItems(int32_t clientSocket)
{
  int num = getItemCount();

  for ( int i=0; i<num; i++ ) {
  	Item item = getItem(i);
    sprintf(responseBuffer, "%2d %-20s %3d kr\n", i, item.name, item.price);
    telnetSend(clientSocket, responseBuffer);
  }
  return true;
}

bool telnetCreditUser(int32_t clientSocket, int numArgs, char *name, int credit)
{
  if ( numArgs < 2 ) {
    telnetSend(clientSocket, "Error: You must specify both name and credit\n");
    return false;
  }

  if ( creditUser(name, credit, tempBuffer) ) {
    sprintf(responseBuffer, "%s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return true;
  } else {
    sprintf(responseBuffer, "Error: %s\n", tempBuffer);
    telnetSend(clientSocket, responseBuffer);
    return false;
  }
}

bool telnetBuyItem(int32_t clientSocket, int numArgs, char *name, int itemId)
{
	int userId;

	if ( numArgs < 2 ) {
		sprintf(responseBuffer, "Error: You must specify user name and item ID\n");
		telnetSend(clientSocket, responseBuffer);
		return false;
	}

	userId = getUserIdByName(name);
	if ( userId == -1 ) {
		sprintf(responseBuffer, "Error: unknown user\n");
		telnetSend(clientSocket, responseBuffer);
		return false;
	}

	if ( buyItem(userId, itemId, tempBuffer) ) {
		sprintf(responseBuffer, "%s\n", tempBuffer);
		telnetSend(clientSocket, responseBuffer);
		return true;
	} else {
		sprintf(responseBuffer, "Error: %s\n", tempBuffer);
		telnetSend(clientSocket, responseBuffer);
		return false;
	}

}

bool telnetHelp(int32_t clientSocket)
{
	telnetSend(clientSocket, "Available commands:\n"
			"  adduser <username>\n"
			"  additem <itemname> <price>\n"
			"  credit <username> <amount>\n"
			"  buy <username> <item id>\n"
			"  listusers\n"
			"  listitems\n"
			"  edituser <id> <name>\n"
			"  edititem <id> <itemname> <price>\n"
			);
}

/* Handle any incoming commands from a telnet client. */
void telnetHandleClient(int32_t clientSocket, sockaddr clientAddr)
{
  int bytesRecvd;

  char cmd[TELNET_MAX_CMD_LEN];
  char name[MAX_NAME_LEN];
  int value;
  int numArgs;
  int index;

  bool quitRequest = false;

  printf("New telnet connection from ");
  printIpAddr(&clientAddr.sa_data[0], true);
  printf("\n");

  uint32_t startTime = msTicks;

  // Set timeout on recv
  struct timeval timeout;
  timeout.tv_sec = 2;
  timeout.tv_usec = 0;
  socklen_t optlen = sizeof(timeout);
  int err = setsockopt(clientSocket, SOL_SOCKET, SOCKOPT_RECV_TIMEOUT, &timeout, optlen);
  if ( err == 0) {
  	int sec = timeout.tv_sec;
  	printf("Setting timeout on client telnet socket to: %d s", sec);
  } else {
  	printf("setsockopt() returned error: %d\n", err);
  }

  telnetSend(clientSocket, "cmd> ");

  while ( !quitRequest ) {

		memset(telnetRequestBuffer,0,sizeof(telnetRequestBuffer));
		bytesRecvd = recv(clientSocket, telnetRequestBuffer, sizeof(telnetRequestBuffer), 0);

		// 30 second idle timeout
		if ( msTicks - startTime > 30000 ) {
			telnetSend(clientSocket, "\nBye!\n");
			break;
		}

		if ( bytesRecvd < 0 ) {
			printf("DEBUG recv error\n");
			break;
		} else if ( bytesRecvd == 0 ) {
			continue;
		} else {
			startTime = msTicks;
		}

		printf("Received %d bytes: %s\n", bytesRecvd, telnetRequestBuffer);

		okToShutDown = false;

		sscanf(telnetRequestBuffer, "%s", cmd);
		if ( !strcmp(cmd, "edititem") || !strcmp(cmd, "edituser")  ) {
			// edit commands are in format <cmd> <index> <name> [value]
			numArgs = sscanf(telnetRequestBuffer, "%s %d %s %d", cmd, &index, name, &value);
		} else {
			// Most commands are in format <cmd> <name> [value]
			numArgs = sscanf(telnetRequestBuffer,"%s %s %d", cmd, name, &value);
		}

		if ( numArgs <= 0 ) {
			printf("Error while parsing command from telnet\n");
			telnetSend(clientSocket, "Unknown command\n");
		} else {
			 if ( !strcmp(cmd, "adduser") ) {
				 telnetAddUser(clientSocket, numArgs-1, name, value);
			 } else if ( !strcmp(cmd, "additem") ) {
				 telnetAddItem(clientSocket, numArgs-1, name, value);
			 } else if ( !strcmp(cmd, "listitems") ) {
				 telnetListItems(clientSocket);
			 } else if ( !strcmp(cmd, "edititem") ) {
				 telnetEditItem(clientSocket, numArgs-1, index, name, value);
			 } else if ( !strcmp(cmd, "edituser") ) {
				 telnetEditUser(clientSocket, numArgs-1, index, name);
			 } else if ( !strcmp(cmd, "credit") ) {
				 telnetCreditUser(clientSocket, numArgs-1, name, value);
			 } else if ( !strcmp(cmd, "listusers") ) {
				 telnetListUsers(clientSocket);
			 } else if ( !strcmp(cmd, "buy") ) {
				 telnetBuyItem(clientSocket, numArgs-1, name, value);
			 } else if ( !strcmp(cmd, "help") ) {
				 telnetHelp(clientSocket);
			 } else if ( !strcmp(cmd, "quit") ) {
				 telnetSend(clientSocket, "Bye!\n");
				 quitRequest = true;
			 } else {
				 telnetSend(clientSocket, "Unknown command\n");
			 }
		}

		telnetSend(clientSocket, "cmd> ");

  }

	uint32_t msStart = msTicks;
	while (!okToShutDown && msTicks < msStart + 1000);

  printf("Closing socket\n");

  closesocket(clientSocket);
}
